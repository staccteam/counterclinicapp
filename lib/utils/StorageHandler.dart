import 'package:shared_preferences/shared_preferences.dart';


/**
 * Delete data from the storage with the key
 */
void deleteText(String key)
{
  final prefs = SharedPreferences.getInstance();
  prefs.then((SharedPreferences _pref){
    _pref.remove(key);
    print("Key:$key removed Successfully!");
  });
}

/**
 * Save data to the storage associated with the key
 */
void saveText(String key, String value) {
  final prefs = SharedPreferences.getInstance();
  prefs.then((SharedPreferences _pref){
    _pref.setString(key, value);
  });
  print("Key: $key Saved Successfully!");
}

/**
 * Fetch value from the storage with the given key
 */
Future<String> getText(String key) async {
  final prefs = await SharedPreferences.getInstance();
  String value = "NULL";
  value = prefs.getString(key);
  print("Retrieved Value From Storage with key: $key");
  
  return value;
}