import 'dart:convert';

import 'package:counter_clinic/utils/StorageHandler.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

const double DEFAULT_PADDING = 5.0;
const double LABEL_FONT_SIZE = 18.0;
const double MEDIUM_LABEL_FONT_SIZE = 24.0;
const double BIG_LABEL_FONT_SIZE = 36.0;
const double BIG_BTN_HEIGHT = 75.0;
const double BIG_BTN_WIDTH = 250.0;
const double MEDIUM_BTN_HEIGHT = 50.0;
const double MEDIUM_BTN_WIDTH = 150.0;
const double SMALL_BTN_HEIGHT = 50.0;
const double SMALL_BTN_WIDTH = 100.0;
const String APPOINTMENT_NUMBER = "appointmentNumber";
const String THIS_PATIENT = "thisPatient";
const String APPOINTMENT_STATUS_KEY = "appointmentStatus";
const String JWT_TOKEN_STORAGE_KEY = "jwtToken";
const String AUTH_TOKEN = "authToken";
const String APPOINTMENT_PAYLOAD_STORAGE_KEY = "appointmentPayload";
const String ACCESS_TOKEN = "accessToken";
const String LOGO_DEFAULT_PATH =  "http://bemyaficionado.com:8080/global/images/logo/site-logo.png";

bool isUserLoggedIn = false;

launchUrl(String url) async 
{
  if  (await canLaunch(url))
  {
    await launch(url);
  }
}

String getJsonValue(String jsonStr,  String key)
{
  Map<String, dynamic> data = json.decode(jsonStr);
  return data[key];
}

String get getThisPatientKey {
  return THIS_PATIENT;
}

String get getAppointmentStatusKey {
  return APPOINTMENT_STATUS_KEY;
}

String formatDate(DateTime dateTime)
{
  return DateFormat("MMM dd',' yyyy")
          .format(dateTime);
}

String formatDatePattern(String pattern, DateTime dateTime)
{
  return DateFormat(pattern).format(dateTime);
}

getScreenWidthInPercent(BuildContext context, double percent){
  return MediaQuery.of(context).size.width*percent;
}

Widget getTextWithFontAndSize({
  @required String text, 
  String fontName = "Roboto",
  double fontSize = LABEL_FONT_SIZE,
  bool softWrap = true,
  Color color = Colors.black,
  double padding = DEFAULT_PADDING,
  Widget prefix,
  int maxLines =  2,
  bool wrapContainer = true
})  
{
  Widget myText = prefix != null ? Row(
        children: <Widget>[
          prefix,
          Text(
            text, 
            style: TextStyle(
              fontFamily: fontName,
              fontSize: fontSize,
              color: color,
            ),
            softWrap: softWrap,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      )  : 
      Text(
        text, 
        maxLines: maxLines,
        style: TextStyle(
          fontFamily: fontName,
          fontSize: fontSize,
          color: color,
        ),
        softWrap: softWrap,
      );
  if (wrapContainer == true)
    return Container(
      padding: EdgeInsets.all(padding),
      child: myText
    );
  
  return myText;
}
