import 'dart:async';
import 'dart:convert';
import 'package:counter_clinic/api/api_constants.dart';
import 'package:counter_clinic/utils/StorageHandler.dart';
import 'package:counter_clinic/utils/Utils.dart';
import 'package:counter_clinic/models/slots.dart';
import 'package:counter_clinic/models/user.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;

class RequestHandler {

  static const String env = "LOCAL";

  Future<String> fetchAppointmentToken(int appointmentId) async {
    Future<http.Response> appointmentToken = http.get(
      Uri.encodeFull( BASE_URL + '/api/fetch-appointment-token/' + appointmentId.toString())
    );
    return appointmentToken.then((http.Response response)=>response.body);
  }

  /**
   * Gives call to the service and returns future with appointment status object 
   */
  Future<String> fetchAppointmentStatus(String jwtToken) async {
    Map<String, dynamic> jsonMap = {
      "appointment": jwtToken,
      "deviceTime" : this.dateTimeFormatter("YYYY-mm-dd hh:mm:ss", new DateTime.now())
    };

    Future<http.Response> response = http.post(
      Uri.encodeFull(BASE_URL + '/api/appointment-stats'),
      body: jsonMap,
      headers: {
        'Content-Type':'application/x-www-form-urlencoded',
      }
    );

    return response.then( (http.Response res) => res.body )
            .catchError((){
              throw("Service returned error!");
            });
  }

  String dateTimeFormatter(String format, DateTime dateTime) {
      var formatter = new DateFormat(format);
      return formatter.format(dateTime);
  }

  Future<String> fetchSiteOption(String siteOption) async {
    print(BASE_URL + '/api/option/$siteOption');
    http.Response response = await http.get(
      Uri.encodeFull( BASE_URL + '/option/$siteOption')
    );

    return response.body;

  }

  Future<String> loginUser(String username, String password) async {
    String loginUrl = '$BASE_URL/api/auth/login';
    print("Sending Request to: $loginUrl");
    http.Response response = await http.post(loginUrl,  body: json.encode({
      'username': username,
      'password': password
    }), headers: {
      "Content-Type":"application/json"
    });

    print("Response Status: ${response.statusCode}");

    if(response.statusCode != 200)
      return "${response.statusCode}";
    return response.body;
  }

  /// fetches user auth data with by using authToken
  Future<String> authUser(String authToken) async
  {
    String authUrl = "$BASE_URL/api/auth?access_token=$authToken";
    print("Sending Request to: $authUrl");
    http.Response response = await http.get(Uri.encodeFull(authUrl));
    
    print("Response Status: ${response.statusCode}");

    if (response.statusCode != 200)
      return "${response.statusCode}";
    return response.body;
  }

  Future<String> registerNewUser(User user) async
  {
    String registerUserUrl  = "$BASE_URL/api/user";
    print("Registering user to: $registerUserUrl");
    http.Response response = await http.post(
      Uri.encodeFull(registerUserUrl), 
      body: json.encode(user.toJson()), 
      headers: {
        "Content-Type":"application/json"
      }
    );
    return response.body;
  }

  Future<String> fetchAllAppointments() async
  {
    String authToken = await getText(AUTH_TOKEN);
    http.Response response = await http.get(Uri.encodeFull("$BASE_URL/api/appointment?access_token=$authToken"));
    return response.body;
  }

  Future<List<User>> fetchAllDoctors() async
  {
    String authToken = await getText(AUTH_TOKEN);
    http.Response  response = await http.get(
      Uri.encodeFull("$BASE_URL/api/user/doctors?access_token=$authToken"));
    print(response.body);
    List<User> doctors = List<User>();
    List<dynamic> parsedJson = json.decode(response.body);
    doctors = parsedJson.map((item)=>User.fromJson(item)).toList();
    return doctors;
  }

  Future<Slots> fetchAllSlots() async
  {
    String token = await getText(AUTH_TOKEN);
    String authToken = getJsonValue(token, ACCESS_TOKEN);
    print("$BASE_URL/api/calendar/all-slots?auth_token=$authToken");
    http.Response response = await http.get(
      Uri.encodeFull("$BASE_URL/calendar/all-slots?auth_token=$authToken"));
    print(response.body);
    List<Slot>  slots = List<Slot>();
    List<dynamic> parsedJson = json.decode(response.body);
    slots =  parsedJson.map((slot)=>Slot.fromJson(slot)).toList();
    Slots slotList = Slots();
    slotList.slot = slots;
    return slotList;
  }
}