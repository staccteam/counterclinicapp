import 'dart:async';

import 'package:simple_permissions/simple_permissions.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';

class QRScanner {

  Future<String> scan() async {
    String barcode = "New Variable";
    print(barcode);
    try {
      requestCameraPermission();
      barcode = await BarcodeScanner.scan();
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
          barcode = 'The user did not grant the camera permission!';
      } else {
          barcode = 'Unknown error: $e';
      }
    } on FormatException{
          barcode = 'null (User returned using the "back"-button before scanning anything. Result)';
    } catch (e) {
          barcode = 'Unknown error: $e';
    }
    print("Barcode Value: " + barcode);
    return barcode;
  }

  requestCameraPermission() async {
    final res = SimplePermissions.requestPermission(Permission.Camera);
    return res;
  }
}