import 'dart:convert';

import 'package:counter_clinic/exceptions/InvalidJwtTokenException.dart';

class JwtParser {

  String header;
  String payload;
  String signature;

  JwtParser(String jwtToken){
    final tokenParts = jwtToken.split('.');

    if  (tokenParts.length != 3)
      throw new InvalidJwtTokenException("Token passed doesn't seems to be an JWT Token.");
      
    print('${tokenParts[0]}\n');
    print('${tokenParts[1]}\n');
    print('${tokenParts[2]}\n');
    
    header = _decodeBase64(tokenParts[0]);
    payload = _decodeBase64(tokenParts[1]);
    // signature  = _decodeBase64(tokenParts[2]);
  }
      
  String _decodeBase64(String str) {
    String output = str.replaceAll('-', '+').replaceAll('_', '/');

    switch (output.length % 4) {
      case 0:
        break;
      case 2:
        output += '==';
        break;
      case 3:
        output += '=';
        break;
      default:
        throw Exception('Illegal base64url string!"');
    }

    return utf8.decode(base64Url.decode(output));
  }
}
      
     
