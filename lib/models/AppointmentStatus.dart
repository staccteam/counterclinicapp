import 'dart:convert';

class AppointmentStatus {
  
  int appointmentId;
  int approxAppointmentTime;
  String clinicNumber;
  String doctorName;
  int doctorId;
  int currQueueCount;
  String doctorStatus;
  int queueSize;
  int totalCheckupTime;
  int avgTime;
  int extraTime;
  int pauseTime;
  int currPatientInTime;
  bool patientInTimeMoreThanAvgWaitTime;
  bool avgTimeSet;

  AppointmentStatus.empty() {
    this.approxAppointmentTime = 5;
    this.avgTime = 5;
    this.currQueueCount = 0;
    this.extraTime = 0;
    this.pauseTime = 0;
    this.avgTimeSet = false;
    this.doctorStatus = "Not Arrived";
    this.queueSize = 0;
  }

  AppointmentStatus.over(int patientNumber) {
    this.approxAppointmentTime = 5;
    this.avgTime = 0;
    this.currQueueCount = patientNumber+1;
    this.extraTime = 0;
    this.pauseTime = 0;
    this.avgTimeSet = false;
    this.doctorStatus = "In Progress";
  }

  AppointmentStatus.fromMap(Map<String, dynamic> jsonMap) {
    this.appointmentId = jsonMap["appointmentId"];
    this.approxAppointmentTime = jsonMap["approxAppointmentTime"];
    this.clinicNumber  = jsonMap["clinicNumber"];
    this.doctorName = jsonMap["doctorName"];
    this.doctorId  = jsonMap["doctorId"];
    this.currQueueCount =  jsonMap["currQueueCount"];
    this.doctorStatus = jsonMap["doctorStatus"];
    this.queueSize = jsonMap["queueSize"];
    this.totalCheckupTime = jsonMap["totalCheckupTime"];
    this.avgTime = jsonMap["avgTime"];
    this.extraTime =  jsonMap["extraTime"];
    this.pauseTime = jsonMap["pauseTime"];
    this.currPatientInTime  = jsonMap["currPatientInTime"];
    this.patientInTimeMoreThanAvgWaitTime  = jsonMap["patientInTimeMoreThanAvgWaitTime"];
    this.avgTimeSet = jsonMap["avgTimeSet"];
  }

  AppointmentStatus.fromJson(String jsonStr) {
    final  _map = jsonDecode(jsonStr);
    this.appointmentId = _map["appointmentId"];
    this.approxAppointmentTime = _map["approxAppointmentTime"];
    this.clinicNumber  = _map["clinicNumber"];
    this.doctorName = _map["doctorName"];
    this.doctorId  = _map["doctorId"];
    this.currQueueCount =  _map["currQueueCount"];
    this.doctorStatus = _map["doctorStatus"];
    this.queueSize = _map["queueSize"];
    this.totalCheckupTime = _map["totalCheckupTime"];
    this.avgTime = _map["avgTime"];
    this.extraTime =  _map["extraTime"];
    this.pauseTime = _map["pauseTime"];
    this.currPatientInTime  = _map["currPatientInTime"];
    this.patientInTimeMoreThanAvgWaitTime  = _map["patientInTimeMoreThanAvgWaitTime"];
    this.avgTimeSet = _map["avgTimeSet"];
  }

  AppointmentStatus.convert(dynamic data) {
    if (data == null || data == "")
      print("Null Object for Convert!");

    else if (data is Map)
      AppointmentStatus.fromMap(data);

    else if (data is String)
      AppointmentStatus.fromJson(data); 
  }

  String toJson() => jsonEncode(this);

  String toString() {
    return """
    
    appointmentId = $appointmentId;
    approxAppointmentTime = $approxAppointmentTime;
    clinicNumber = $clinicNumber;
    doctorName = $doctorName;
    doctorId = $doctorId;
    currQueueCount = $currQueueCount;
    doctorStatus = $doctorStatus;
    queueSize = $queueSize;
    totalCheckupTime = $totalCheckupTime;
    avgTime = $avgTime;
    extraTime = $extraTime;
    pauseTime = $pauseTime;
    currPatientInTime = $currPatientInTime;
    patientInTimeMoreThanAvgWaitTime = $patientInTimeMoreThanAvgWaitTime;
    avgTimeSet = $avgTimeSet;


    """;
  }
}