class Slots {
  List<Slot> slot;

  Slots({this.slot});

  Slots.fromJson(List<dynamic> json) {
    if (json != null) {
      slot = new List<Slot>();
      json.forEach((v) {
        slot.add(new Slot.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.slot != null) {
      data['slot'] = this.slot.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Slot {
  String id;
  String startTime;
  String endTime;
  String whatFor;

  Slot({this.id, this.startTime, this.endTime, this.whatFor});

  Slot.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    startTime = json['startTime'];
    endTime = json['endTime'];
    whatFor = json['whatFor'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['startTime'] = this.startTime;
    data['endTime'] = this.endTime;
    data['whatFor'] = this.whatFor;
    return data;
  }
}