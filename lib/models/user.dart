class User {
  String id;
  String firstName;
  String lastName;
  String email;
  String mobile;
  String username;
  String password;
  List<Roles> roles;
  Clinic clinic;
  AppointmentQueue appointmentQueue;
  Calendar calendar;

  User(
      {this.id,
      this.firstName,
      this.lastName,
      this.email,
      this.mobile,
      this.username,
      this.password,
      this.roles,
      this.clinic,
      this.appointmentQueue,
      this.calendar});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    email = json['email'];
    mobile = json['mobile'];
    username = json['username'];
    password = json['password'];
    if (json['roles'] != null) {
      roles = new List<Roles>();
      json['roles'].forEach((v) {
        roles.add(new Roles.fromJson(v));
      });
    }
    clinic =
        json['clinic'] != null ? new Clinic.fromJson(json['clinic']) : null;
    appointmentQueue = json['appointmentQueue'] != null
        ? new AppointmentQueue.fromJson(json['appointmentQueue'])
        : null;
    calendar = json['calendar'] != null
        ? new Calendar.fromJson(json['calendar'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['username'] = this.username;
    data['password'] = this.password;
    if (this.roles != null) {
      data['roles'] = this.roles.map((v) => v.toJson()).toList();
    }
    if (this.clinic != null) {
      data['clinic'] = this.clinic.toJson();
    }
    if (this.appointmentQueue != null) {
      data['appointmentQueue'] = this.appointmentQueue.toJson();
    }
    if (this.calendar != null) {
      data['calendar'] = this.calendar.toJson();
    }
    return data;
  }
}

class Roles {
  String name;
  List<Permissions> permissions;

  Roles({this.name, this.permissions});

  Roles.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    if (json['permissions'] != null) {
      permissions = new List<Permissions>();
      json['permissions'].forEach((v) {
        permissions.add(new Permissions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    if (this.permissions != null) {
      data['permissions'] = this.permissions.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Permissions {
  String name;

  Permissions({this.name});

  Permissions.fromJson(Map<String, dynamic> json) {
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    return data;
  }
}

class Clinic {
  String id;
  String clinicNumber;

  Clinic({this.id, this.clinicNumber});

  Clinic.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    clinicNumber = json['clinicNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['clinicNumber'] = this.clinicNumber;
    return data;
  }
}

class AppointmentQueue {
  String id;
  String doctorId;
  int queueSize;
  int currQueueCount;
  int extendedTime;
  int avgCheckupTime;
  int patientInTimeStamp;
  int totalCheckupTime;
  String doctorStatus;

  AppointmentQueue(
      {this.id,
      this.doctorId,
      this.queueSize,
      this.currQueueCount,
      this.extendedTime,
      this.avgCheckupTime,
      this.patientInTimeStamp,
      this.totalCheckupTime,
      this.doctorStatus});

  AppointmentQueue.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    doctorId = json['doctorId'];
    queueSize = json['queueSize'];
    currQueueCount = json['currQueueCount'];
    extendedTime = json['extendedTime'];
    avgCheckupTime = json['avgCheckupTime'];
    patientInTimeStamp = json['patientInTimeStamp'];
    totalCheckupTime = json['totalCheckupTime'];
    doctorStatus = json['doctorStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['doctorId'] = this.doctorId;
    data['queueSize'] = this.queueSize;
    data['currQueueCount'] = this.currQueueCount;
    data['extendedTime'] = this.extendedTime;
    data['avgCheckupTime'] = this.avgCheckupTime;
    data['patientInTimeStamp'] = this.patientInTimeStamp;
    data['totalCheckupTime'] = this.totalCheckupTime;
    data['doctorStatus'] = this.doctorStatus;
    return data;
  }
}

class Calendar {
  String calendarId;
  String summary;
  String kind;
  String timezone;

  Calendar({this.calendarId, this.summary, this.kind, this.timezone});

  Calendar.fromJson(Map<String, dynamic> json) {
    calendarId = json['calendarId'];
    summary = json['summary'];
    kind = json['kind'];
    timezone = json['timezone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['calendarId'] = this.calendarId;
    data['summary'] = this.summary;
    data['kind'] = this.kind;
    data['timezone'] = this.timezone;
    return data;
  }
}
