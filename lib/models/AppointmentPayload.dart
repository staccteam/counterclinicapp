import 'dart:convert';

import 'package:counter_clinic/models/AppointmentVO.dart';

class AppointmentPayload {
  dynamic iat;
  String sub;
  dynamic exp;
  String aud;
  String iss;
  AppointmentVO data;

  AppointmentPayload.fromJson(String jsonStr)  {
    final  _map = jsonDecode(jsonStr);
    this.iat =  _map["iat"];
    this.sub = _map["sub"];
    this.exp = _map["exp"];
    this.aud = _map["aud"];
    this.iss = _map["iss"];
    this.data = AppointmentVO.fromJson(_map['data']);
  }

  AppointmentPayload.fromMap(Map<String, dynamic> jsonMap) {
    this.iat =  jsonMap["iat"];
    this.sub = jsonMap["sub"];
    this.exp = jsonMap["exp"];
    this.aud = jsonMap["aud"];
    this.iss = jsonMap["iss"];
    this.data = AppointmentVO.fromJson(jsonMap['data']);
  }

  AppointmentPayload.convert(dynamic data) {
    if (data == null || data == "")
      print("Null Object for Convert!");

    else if (data is Map)
      AppointmentPayload.fromMap(data);

    else if (data is String)
      AppointmentPayload.fromJson(data); 
  }

  Map<String, dynamic> toJson() => 
  {
    'iat': this.iat,
    'sub': this.sub,
    'exp': this.exp,
    'aud': this.aud,
    'iss': this.iss,
    'data': this.data.toJson()
  };
}