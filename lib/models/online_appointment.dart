import 'package:counter_clinic/models/user.dart';

class OnlineAppointment {
  User patient;
  User doctor;
  TimeSlot slot;
  String date;

  OnlineAppointment({this.patient, this.doctor, this.slot, this.date});

  OnlineAppointment.fromJson(Map<String, dynamic> json) {
    patient =
        json['patient'] != null ? new User.fromJson(json['patient']) : null;
    doctor =
        json['doctor'] != null ? new User.fromJson(json['doctor']) : null;
    slot = json['slot'] != null ? new TimeSlot.fromJson(json['slot']) : null;
    date = json['date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.patient != null) {
      data['patient'] = this.patient.toJson();
    }
    if (this.doctor != null) {
      data['doctor'] = this.doctor.toJson();
    }
    if (this.slot != null) {
      data['slot'] = this.slot.toJson();
    }
    data['date'] = this.date;
    return data;
  }
}

class TimeSlot {
  String id;
  String startTime;
  String endTime;
  String whatFor;

  TimeSlot({this.id, this.startTime, this.endTime, this.whatFor});

  TimeSlot.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    startTime = json['startTime'];
    endTime = json['endTime'];
    whatFor = json['whatFor'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['startTime'] = this.startTime;
    data['endTime'] = this.endTime;
    data['whatFor'] = this.whatFor;
    return data;
  }
}