class SiteOptions {
  List<SiteOption> siteOptionList;

  SiteOptions({this.siteOptionList});

  SiteOptions.fromJson(List<dynamic> json) {
    if (json != null) {
      siteOptionList = new List<SiteOption>();
      json.forEach((v) {
        siteOptionList.add(new SiteOption.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.siteOptionList != null) {
      data['siteOption'] = this.siteOptionList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SiteOption {
  String id;
  String key;
  String val;

  SiteOption({this.id, this.key, this.val});

  SiteOption.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    key = json['key'];
    val = json['val'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['key'] = this.key;
    data['val'] = this.val;
    return data;
  }
}