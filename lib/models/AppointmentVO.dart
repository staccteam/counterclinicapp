import 'dart:convert';

class AppointmentVO
{
  int appointmentId;
  String appointmentToken;
  String patientName;
  int doctorId;
  String doctorName;
  int currQueueCount;
  String qrCodeName;
  int clinicRoomId;
  String clilnicRoomNumber;
  String appointmentCreated;

  AppointmentVO.fromJson(String jsonStr) {
    print("Decoding json called of Appointment VO");
    dynamic _map = jsonDecode(jsonStr);
    this.appointmentId =  _map['appointmentId'];
    this.patientName  = _map['patientName'];
    this.doctorId =  _map['doctorId'];
    this.doctorName  = _map['doctorName'];
    this.currQueueCount  = _map['currQueueCount'];
    this.qrCodeName = _map['qrCodeName'];
    this.clinicRoomId = _map['clinicRoomId'];
    this.clilnicRoomNumber = _map['clinicRoomNumber'];
    this.appointmentCreated  =  _map['appointmentCreated'];
  }

  AppointmentVO.fromMap(Map<String, dynamic> jsonMap) {
    this.appointmentId =  jsonMap['appointmentId'];
    this.patientName  = jsonMap['patientName'];
    this.doctorId =  jsonMap['doctorId'];
    this.doctorName  = jsonMap['doctorName'];
    this.currQueueCount  = jsonMap['currQueueCount'];
    this.qrCodeName = jsonMap['qrCodeName'];
    this.clinicRoomId = jsonMap['clinicRoomId'];
    this.clilnicRoomNumber = jsonMap['clinicRoomNumber'];
    this.appointmentCreated  =  jsonMap['appointmentCreated'];
  }

  AppointmentVO.convert(dynamic data) {
    if (data == null || data == "")
      print("Null Object for Convert!");

    else if (data is Map)
      AppointmentVO.fromMap(data);

    else if (data is String)
      AppointmentVO.fromJson(data); 
  }

  Map<String,  dynamic>  toJson()  =>
  {
    'appointmentId': this.appointmentId,
    'patientName': this.patientName,
    'doctorId': this.doctorId,
    'doctorName': this.doctorName,
    'currQueueCount': this.currQueueCount,
    'qrCodeName' : this.qrCodeName,
    'clinicRoomId': this.clinicRoomId,
    'clinicRoomNumber':  this.clilnicRoomNumber,
    'appointmentCreated': this.appointmentCreated
  };
}