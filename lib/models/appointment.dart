class Appointment {
  String id;
  String appointmentQueueId;
  dynamic clinic;
  String doctorId;
  String patientName;
  String doctorName;
  int appointmentNumber;
  String qrCodeName;
  String qrCodeImagePath;
  String qrCodeImageUrl;
  String uniqueToken;
  String appointmentTime;
  String appointmentType;
  int appointmentTimeStamp;

  Appointment(
      {this.id,
      this.appointmentQueueId,
      this.clinic,
      this.doctorId,
      this.patientName,
      this.doctorName,
      this.appointmentNumber,
      this.qrCodeName,
      this.qrCodeImagePath,
      this.qrCodeImageUrl,
      this.uniqueToken,
      this.appointmentTime,
      this.appointmentType,
      this.appointmentTimeStamp});

  Appointment.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    appointmentQueueId = json['appointmentQueueId'];
    clinic = json['clinic'];
    doctorId = json['doctorId'];
    patientName = json['patientName'];
    doctorName = json['doctorName'];
    appointmentNumber = json['appointmentNumber'];
    qrCodeName = json['qrCodeName'];
    qrCodeImagePath = json['qrCodeImagePath'];
    qrCodeImageUrl = json['qrCodeImageUrl'];
    uniqueToken = json['uniqueToken'];
    appointmentTime = json['appointmentTime'];
    appointmentType = json['appointmentType'];
    appointmentTimeStamp = json['appointmentTimeStamp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['appointmentQueueId'] = this.appointmentQueueId;
    data['clinic'] = this.clinic;
    data['doctorId'] = this.doctorId;
    data['patientName'] = this.patientName;
    data['doctorName'] = this.doctorName;
    data['appointmentNumber'] = this.appointmentNumber;
    data['qrCodeName'] = this.qrCodeName;
    data['qrCodeImagePath'] = this.qrCodeImagePath;
    data['qrCodeImageUrl'] = this.qrCodeImageUrl;
    data['uniqueToken'] = this.uniqueToken;
    data['appointmentTime'] = this.appointmentTime;
    data['appointmentType'] = this.appointmentType;
    data['appointmentTimeStamp'] = this.appointmentTimeStamp;
    return data;
  }
}


class AppointmentQueueStatus {
  String id;
  String doctorId;
  int queueSize;
  int currQueueCount;
  int extendedTime;
  int avgCheckupTime;
  int patientInTimeStamp;
  int totalCheckupTime;
  String averageTimeCalculationLogic;
  String doctorStatus;

  AppointmentQueueStatus(
      {this.id,
      this.doctorId,
      this.queueSize,
      this.currQueueCount,
      this.extendedTime,
      this.avgCheckupTime,
      this.patientInTimeStamp,
      this.totalCheckupTime,
      this.averageTimeCalculationLogic,
      this.doctorStatus});

  AppointmentQueueStatus.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    doctorId = json['doctorId'];
    queueSize = json['queueSize'];
    currQueueCount = json['currQueueCount'];
    extendedTime = json['extendedTime'];
    avgCheckupTime = json['avgCheckupTime'];
    patientInTimeStamp = json['patientInTimeStamp'];
    totalCheckupTime = json['totalCheckupTime'];
    averageTimeCalculationLogic = json['averageTimeCalculationLogic'];
    doctorStatus = json['doctorStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['doctorId'] = this.doctorId;
    data['queueSize'] = this.queueSize;
    data['currQueueCount'] = this.currQueueCount;
    data['extendedTime'] = this.extendedTime;
    data['avgCheckupTime'] = this.avgCheckupTime;
    data['patientInTimeStamp'] = this.patientInTimeStamp;
    data['totalCheckupTime'] = this.totalCheckupTime;
    data['averageTimeCalculationLogic'] = this.averageTimeCalculationLogic;
    data['doctorStatus'] = this.doctorStatus;
    return data;
  }
}

