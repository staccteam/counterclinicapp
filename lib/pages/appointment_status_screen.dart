import 'dart:async';
import 'dart:convert';

import 'package:counter_clinic/pages/Screens.dart';
import 'package:counter_clinic/partials/view_components.dart';
import 'package:counter_clinic/utils/RequestHandler.dart';
import 'package:counter_clinic/utils/StorageHandler.dart';
import 'package:counter_clinic/utils/Utils.dart';
import 'package:counter_clinic/models/AppointmentPayload.dart';
import 'package:counter_clinic/models/AppointmentStatus.dart';
import 'package:flutter/material.dart';

  var counterTime = 0;

class AppointmentStatusScreen extends StatefulWidget {

  String jwtToken;
  AppointmentPayload hisAppointmentStatus;

  AppointmentStatusScreen({Key key, @required this.jwtToken, @required this.hisAppointmentStatus}): super(key: key){
    currentDrawerState = Screens.APPOINTMENT_STATUS;
    saveText(JWT_TOKEN_STORAGE_KEY, jwtToken);
  }

  @override
  _AppointmentStatusScreenState createState() => _AppointmentStatusScreenState();
}

class _AppointmentStatusScreenState extends State<AppointmentStatusScreen> {
  final FIVE_MINUTES_IN_SECONDS = 300;
  final ONE_MINUTE_IN_SECONDS = 60;
  final number = ValueNotifier(0);

  AppointmentStatus prevAppointmentState;
  AppointmentStatus currentAppointmentState;
  int _timeRemaining = 0;  //  in seconds
  RequestHandler _httpHandler;
  Timer _timer;
  bool shouldShowMessage = false;
  String showMessage;
  bool _isDisposed = false;
  int _seconds = 0;

  @override
  void initState()  {
    super.initState();
    _isDisposed  = false;
    _httpHandler = RequestHandler();
    currentAppointmentState = AppointmentStatus.empty();
    _refreshCurrentAppointmentState()
      .then((onValue){
        if (! _isDisposed)
        setState(() {
          prevAppointmentState = currentAppointmentState;
          _refreshTimeRemaining();  
        });
        _refreshWidgetState();
        execEverySecond();
      });

  }

  void dispose() {
    super.dispose();
    _isDisposed = true;
    _timer.cancel();
  }

  execEverySecond() async {
    _timer = Timer.periodic(Duration(seconds: 1), (Timer t)  async {
      _seconds++;
      // refresh current appointment state after every 5 seconds.
      if (_seconds % 5 == 0)
      {
        await _refreshCurrentAppointmentState();
        _seconds = 0;
      }
        
      // _refreshTimeRemaining();
      _refreshWidgetState();

    });
  }

  /**
   * Makes a service call to retrieve appointment status
   * and updates the current appointment state accordingly
   */
  Future<void> _refreshCurrentAppointmentState() async {
    // make service call to fetch the current appointment state
    Map<String, dynamic> _map = jsonDecode(await _httpHandler.fetchAppointmentStatus(widget.jwtToken));
    if (_map.containsKey("data"))
      currentAppointmentState = AppointmentStatus.fromMap(
        _map["data"]
      );
    
    else if (_map.containsKey("status"))
    {
      if (_map["status"] == 500)
      {
        currentAppointmentState = AppointmentStatus.over(widget.hisAppointmentStatus.data.currQueueCount);
      }
    }

    print("Current Appointment State: ${currentAppointmentState.toString()}");
  }

  _refreshTimeRemaining() async {

    if (counterTime > 0 && counterTime < (prevAppointmentState.avgTime*ONE_MINUTE_IN_SECONDS))
    {
      print("Adding extra time to the time.");
      _timeRemaining =  (prevAppointmentState.avgTime*ONE_MINUTE_IN_SECONDS) + FIVE_MINUTES_IN_SECONDS;
      number.value = _timeRemaining;
      return;
    }
      
    // 5-1 = 4 * 240
    _timeRemaining  = (widget.hisAppointmentStatus.data.currQueueCount - prevAppointmentState.currQueueCount)
                            * (prevAppointmentState.avgTime * ONE_MINUTE_IN_SECONDS);

    _timeRemaining += (prevAppointmentState.extraTime * ONE_MINUTE_IN_SECONDS)
        + (prevAppointmentState.pauseTime * ONE_MINUTE_IN_SECONDS); //in seconds

    number.value = _timeRemaining;
  }

  
  /**
   * Contains the service sync logic
   */
  _refreshWidgetState() async {
    print("Time Remaining (sec): $counterTime : ${number.value} : AvgTime: ${(prevAppointmentState.avgTime*ONE_MINUTE_IN_SECONDS)}");

    if (counterTime > 0 && counterTime < (prevAppointmentState.avgTime*ONE_MINUTE_IN_SECONDS))
    {
      print("Value of counter is less than the average!");
      _refreshTimeRemaining();
    }
    

    // Timer should not start until first patient is taken in
    else if (prevAppointmentState.currQueueCount == 0)
    {
      print("not start until first patient is taken in");
      if (! _isDisposed)
        setState(() {
          prevAppointmentState = currentAppointmentState;
          shouldShowMessage = true;
          showMessage = "Doctor has not started taking patients.";
          _refreshTimeRemaining();
        });
    }

    // It should print the message 'It's your turn'
    else if (widget.hisAppointmentStatus.data.currQueueCount == currentAppointmentState.currQueueCount)
    {
      print("It's your turn");
      if (! _isDisposed)
        setState(() {
          prevAppointmentState = currentAppointmentState;
          shouldShowMessage = true;
          showMessage = "It's your turn!";       
          _refreshTimeRemaining(); 
        });
    }

    // It should print the message 'Your turn is over, please contact receptionist.'
    else if (widget.hisAppointmentStatus.data.currQueueCount < prevAppointmentState.currQueueCount)
    {
      print("Your turn is over, please contact receptionist.");
      if (! _isDisposed)
        setState(() {
          prevAppointmentState = currentAppointmentState;
          shouldShowMessage = true;
          showMessage = "Your turn is over, please contact receptionist.";       
          _refreshTimeRemaining(); 
        });
    }

    // It should print the message 'You are next, be ready!' and not the timer
    else if (widget.hisAppointmentStatus.data.currQueueCount - currentAppointmentState.currQueueCount == 1) 
    {
      print("You are next, be ready!");
      if (! _isDisposed)
        setState(() {
          prevAppointmentState = currentAppointmentState;
          shouldShowMessage = true;
          showMessage = "You are next, be ready!";       
          _refreshTimeRemaining(); 
        });
    }

    // On pressing next patient the currQueue will change and so does the timer
    else if (prevAppointmentState.currQueueCount != currentAppointmentState.currQueueCount) 
    {
      print("On pressing next patient the currQueue will change and so does the timer");
      prevAppointmentState = currentAppointmentState;
      shouldShowMessage = false;
      if (! _isDisposed)
        setState(() {
          _refreshTimeRemaining();        
        });
    }


    // It should sync timer when doctor status changes
    else if (prevAppointmentState.doctorStatus != currentAppointmentState.doctorStatus)  
    {
      print("It should sync timer when doctor status changes!");
      if (! _isDisposed)
        setState(() {
          prevAppointmentState = currentAppointmentState;
          shouldShowMessage = false;     
          _refreshTimeRemaining();   
        });
    }

    //  It should sync timer when the avg time calculation mode changes
    else if (prevAppointmentState.avgTimeSet != currentAppointmentState.avgTimeSet) 
    {
      print("It should sync timer when the avg time calculation mode changes");
      if (! _isDisposed)
        setState(() {
          prevAppointmentState = currentAppointmentState;
          shouldShowMessage = false;     
          _refreshTimeRemaining();   
        });
    }

    // It should show message that doctor has logged out and the day has been ended
    else if (currentAppointmentState.doctorStatus.contains("Logged Out")) {
      print("Doctor has clicked logged out button and have been logged out.");
      if (!  _isDisposed)
        setState(() {
          prevAppointmentState = currentAppointmentState;    
          shouldShowMessage = true;
          showMessage = "Doctor has left for the day!";      
        });
    }
  }
  

  @override
  Widget build(BuildContext context) {
    print("Refresing Build()");
    
    return Scaffold(
        appBar: AppBar(
          title: Text("Appointment Summary"),
        ),
        // drawer: getDrawer(context),
        body: Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/blue_grafica_design_background.jpg'),
                      fit: BoxFit.fill
                    )
                  ),
                ),
                Column(
                  children: <Widget>[
                    Expanded(
                      child: GridView.count(
                        crossAxisCount: 2,
                        childAspectRatio: 8.0,
                        padding:  const EdgeInsets.all(4.0),
                        mainAxisSpacing: 4.0,
                        crossAxisSpacing: 4.0,
                        children: <Widget>[
                          getTextWithFontAndSize(text: "Doctor Name"),
                          getTextWithFontAndSize(prefix: Icon(Icons.person_outline) , text: widget.hisAppointmentStatus.data.doctorName),
                          getTextWithFontAndSize(text: "Room Number"),
                          getTextWithFontAndSize(prefix: Icon(Icons.label_outline), text: widget.hisAppointmentStatus.data.clilnicRoomNumber),
                          getTextWithFontAndSize(text: "Your No."),
                          getTextWithFontAndSize(prefix: Icon(Icons.confirmation_number), text: widget.hisAppointmentStatus != null ? widget.hisAppointmentStatus.data.currQueueCount.toString() : 'loading...'),
                          getTextWithFontAndSize(text: "Current No."),
                          getTextWithFontAndSize(prefix: Icon(Icons.confirmation_number), text: prevAppointmentState != null ? prevAppointmentState.currQueueCount.toString() : 'loading...'),
                          getTextWithFontAndSize(text: "Doctor Status"),
                          getTextWithFontAndSize(prefix: Icon(Icons.star_border), text: prevAppointmentState !=  null ? prevAppointmentState.doctorStatus : 'loading...'),
                        ],
                      ),
                    ),
                    ValueListenableBuilder<int>(
                      valueListenable: number,
                      builder: (context, value, child){
                        print("Number Value is getting printed!");
                        print(value);

                        if (shouldShowMessage)
                          return Expanded(
                            child: Container(
                              width: getScreenWidthInPercent(context, 0.8),
                              child: Column(
                                children: <Widget>[
                                  Icon(
                                    Icons.info,
                                    color: Colors.deepOrange,
                                    size: BIG_BTN_HEIGHT,
                                  ),
                                  Text(
                                    showMessage,
                                    style: TextStyle(
                                      fontFamily: "Roboto",
                                      fontSize: BIG_LABEL_FONT_SIZE
                                    ),
                                  ),
                                ],
                              )
                            )
                          );

                        else
                          return Expanded(
                            child: Container(
                              child: Counter(number: number)
                            ),
                          );
                      },
                    )
                  ],
              ),
              ]
        )
      );
  }
}













class Counter extends StatefulWidget {

  ValueNotifier<int> number;

  Counter({Key key, @required this.number}):super(key: key);

  @override
  _CounterState createState() => _CounterState();
}

class _CounterState extends State<Counter> {

  Timer _timer;
  String formattedTime;

  @override
  initState() {
    super.initState();
    formattedTime  = "Loading...";
    widget.number.addListener(didValueChange);
    print("CounterState: Time Remaining: ${widget.number.value}");
    counterTime = widget.number.value;
    startTimer();
  }

  didValueChange() {
    print("Value did changed!");
    final newValue = widget.number.value;
    if (newValue != counterTime)
    {
      setState(() {
        counterTime = newValue;
      });
    }

    print("New Time: $counterTime");
  }
  

  @override
  dispose() {
    super.dispose();
    widget.number.removeListener(didValueChange);
    _timer.cancel();
  }

  startTimer() {
    _timer  = Timer.periodic(Duration(seconds: 1), (Timer t) {
      _decrementTimeRemaining();
    });
  }

  /**
   * decrements counter by 1 seconds
   * and if the counter reaches below avg time
   * then sets the counter by adding 5 minutes to the avg time
   */
  void _decrementTimeRemaining() {
    
    setState(() {
          counterTime--;
        });
    print("Time Remaining: $counterTime");
  }

  Widget getTextBoxWithFormattedRemainingTime() {
    // return "All Good Here!";
    formattedTime = Duration(seconds: counterTime).toString().substring(0, 7);
    return getTextWithFontAndSize(text: formattedTime, fontSize: BIG_LABEL_FONT_SIZE);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        getTextBoxWithFormattedRemainingTime()
      ],
    );
  }
}