import 'dart:convert';

import 'package:counter_clinic/pages/appointment_status_page.dart';
import 'package:counter_clinic/partials/view_components.dart';
import 'package:counter_clinic/utils/QRScanner.dart';
import 'package:counter_clinic/utils/Utils.dart';
import 'package:flutter/material.dart';

class ScanQrCodePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    Widget _backgroundImage()
    {
      return Container(
        decoration: BoxDecoration(
          // image: DecorationImage(
          //   image: AssetImage('assets/blue_grafica_design_background.jpg'),
          //   fit: BoxFit.fill
          // )
          color: Colors.white70
        ),
      );
    }

    Widget _qrCodeScanImageWithButton()
    {
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            height: 300.0,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/qr-code-placeholder.jpeg'),
                fit: BoxFit.fitHeight
              )
            ),
          ),
          Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(10.0),
            child: Text("Scan QR Code and get your Appointment Details.",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 26.0,
                fontWeight: FontWeight.w700,
                fontFamily: "Roboto",
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RaisedButton(
                    color: Colors.green,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text("SCAN QR CODE",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                          fontSize: 26.0,
                          fontWeight: FontWeight.w700,
                          fontFamily: "Roboto",
                          color: Colors.white70
                        ),
                      ),
                    ),
                    onPressed: () async {
                        QRScanner scanner = new QRScanner();
                        String scannedText = await scanner.scan();
                        if  (scannedText.isNotEmpty)
                        {
                          print("Appointment Number is not empty: $scannedText");
                          int appointmentNumber = json.decode(scannedText)['appointmentId'];
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => 
                              new AppointmentStatusPage(
                                appointmentNumber: appointmentNumber)));
                          
                          return;
                        }
                        Navigator.pop(context);
                    },
                  ),
                ),
              )
            ],
          )
        ],
      );
    }

    Widget layout()
    {
      return Stack(
        children: <Widget>[
          _backgroundImage(),
          _qrCodeScanImageWithButton(),
        ],
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Appointment Status",
          style: TextStyle(
            fontSize: 26.0,
            color: Colors.white
          ),
        ),
      ),
      drawer: getDrawer(context),
      body: Container(
        child: layout(),
      ),
    );
  }
}