import 'package:counter_clinic/blocs/site_option_bloc.dart';
import 'package:counter_clinic/models/site_options.dart';
import 'package:counter_clinic/utils/Utils.dart';
import 'package:counter_clinic/widgets/bloc_provider.dart';
import 'package:flutter/material.dart';

class SupportScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    SiteOptionBloc siteOptionBloc = new SiteOptionBloc();

    Widget layout()
    {
      Widget _backgroundImage()
      {
        return Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/blue_grafica_design_background.jpg'),
              fit: BoxFit.fill
            )
          ),
        );
      }

      Widget _contactCard()
      {
        siteOptionBloc.fetchAllSiteOptions();

        SiteOption _fetchContactEmailOption(SiteOptions siteOptions)
        {
            return siteOptions.siteOptionList.firstWhere((siteOption){
              return siteOption.key == "support_email";
            });
        }

        SiteOption _fetchContactPhoneOption(SiteOptions siteOptions)
        {
            return siteOptions.siteOptionList.firstWhere((siteOption){
              return siteOption.key == "support_phone";
            });
        }

        return StreamBuilder<SiteOptions>(
          stream: siteOptionBloc.siteOptionsStream,
          builder: (context, snapshot){

            if (snapshot.hasData)
            {
              String _supportEmail = _fetchContactEmailOption(snapshot.data).val;
              String _supportPhone = _fetchContactPhoneOption(snapshot.data).val;
              return Card(
                child: Column(
                  children: <Widget>[
                    ListTile(
                      leading: Icon(Icons.email),
                      title: Text(_supportEmail),
                      onTap: (){launchUrl("mailto:$_supportEmail?subject=Need Help");},
                    ),
                    ListTile(
                      leading: Icon(Icons.phone_android),
                      title: Text(_supportPhone),
                      onTap: (){launchUrl("tel:$_supportPhone");},
                    )
                  ],
                ),
              );
            }

            return Center(
              child: CircularProgressIndicator(),
            );
          },
        );
      }

      return Scaffold(
        appBar: AppBar(
          title: Text("Contact Info")
        ),
        // drawer: getDrawer(context),
        body: Stack(
          children: <Widget>[
            _backgroundImage(),
            _contactCard(),
          ],
        ),
          
      );
    }
    return BlocProvider(
      bloc: siteOptionBloc,
      child: layout(),
    );
  }
}