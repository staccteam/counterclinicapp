import 'package:counter_clinic/blocs/create_appointment_bloc.dart';
import 'package:counter_clinic/blocs/users_bloc.dart';
import 'package:counter_clinic/pages/appointment_receipt.dart';
import 'package:counter_clinic/partials/view_components.dart';
import 'package:counter_clinic/utils/Utils.dart';
import 'package:counter_clinic/models/slots.dart';
import 'package:counter_clinic/models/user.dart';
import 'package:counter_clinic/widgets/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:counter_clinic/pages/checkout_screen.dart';
import 'package:intl/intl.dart';

class CreateAppointment extends StatefulWidget {

  @override
  _CreateAppointmentState createState() => _CreateAppointmentState();
}

class _CreateAppointmentState extends State<CreateAppointment> {


  DateTime selectedDate = DateTime.now();

  

  @override
  Widget build(BuildContext context) {
    final CreateAppointmentBloc appointmentBloc = new CreateAppointmentBloc();    

    Future<Null> _selectDate(BuildContext context) async {
      final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
      // if (picked != null && picked != selectedDate)
        appointmentBloc.setDate(picked);
    }

    Widget chooseDoctor() 
    {
      appointmentBloc.fetchUsers();

      return StreamBuilder<AppointmentPageViewData>(
        stream: appointmentBloc.outputMainStream,
        builder: (context, snapshot){
          if (snapshot.hasData && snapshot.data.doctors != null)
          {
            return Column(
              children: <Widget>[
                Text("Choose your doctor", style: Theme.of(context).textTheme.display1,),
                DropdownButton<User>(
                  style: TextStyle(
                      fontSize: BIG_LABEL_FONT_SIZE,
                      color: Colors.green
                    ),
                  value: snapshot.data.selectedDoctor,
                  items: snapshot.data.doctors.map((doctor)=> 
                    DropdownMenuItem<User>(
                      child: Text("${doctor.firstName}", style: Theme.of(context).textTheme.display1),
                      value: doctor,
                    )).toList(),
                    onChanged: (User selectedUser){
                      print("${selectedUser.firstName}");
                      appointmentBloc.updateSelectedDoctor(selectedUser);
                    },
                  )
              ],
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      );
    }

    Widget datePicker = StreamBuilder<AppointmentPageViewData>(
      stream: appointmentBloc.outputMainStream,
      builder: (context, snapshot){
        if (snapshot.hasData)
        {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              snapshot.data.selectedDate == null ? 
                Text("Pick Appointment Date", style: Theme.of(context).textTheme.display1,)
                : Text("${formatDate(snapshot.data.selectedDate)}", 
                    style: TextStyle(
                      fontSize: BIG_LABEL_FONT_SIZE,
                      color: Colors.green
                    ),
                    softWrap: true,
                  ),
              RaisedButton(
                  textTheme: ButtonTextTheme.normal,
                  child: Text("Pick a date", style: Theme.of(context).textTheme.display1,),
                  onPressed: (){
                    _selectDate(context);
                  },
                )
            ],
          );
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );    

    Widget timePicker()
    {
      appointmentBloc.fetchSlots();

      return StreamBuilder<AppointmentPageViewData>(
        stream: appointmentBloc.outputMainStream,
        builder: (context, snapshot){
          if  (snapshot.hasData && snapshot.data.timeSlots != null)
          {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Pick Time Slot", style: Theme.of(context).textTheme.display1),
                Center(
                  child: DropdownButton<Slot>(
                    style: TextStyle(
                      fontSize: BIG_LABEL_FONT_SIZE,
                      color: Colors.green
                    ),
                    value: snapshot.data.selectedTimeSlot,
                    items: snapshot.data.timeSlots.slot.map((slot)=>
                      DropdownMenuItem<Slot>(
                        child: Text("${slot.startTime}", style: Theme.of(context).textTheme.display1,),
                        value: slot,
                      )).toList(),
                    onChanged: (Slot slot){
                      print("${slot.startTime}");
                      appointmentBloc.updateSelectedTimeSlot(slot);
                    },
                  )
                ),
              ],

            );
          }
          return Center(
            child: CircularProgressIndicator(),
          ); 
        },
      );
    }

    Widget submitBtn = RaisedButton(
      textTheme: ButtonTextTheme.primary,
      child: Text("Book Appointment", style: Theme.of(context).textTheme.display1,),
      onPressed: ()async{
        
        showDialog(
          context: context,
          builder: (context){
            return SimpleDialog(
              title: Text("Waiting for the response..."),
              children: <Widget>[
                CircularProgressIndicator()
              ],
            );
          }
        );
        
        bool isSuccess = await appointmentBloc.bookAppointment();

        Navigator.of(context, rootNavigator: true).pop(); // close loading dialog

        if (isSuccess)
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => new AppointmentReceiptPage(viewData: appointmentBloc.viewData)));
        else
          Navigator.pop(context);
      },
    );

    Widget layout = Center(
      child: SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          // choose doctor
          chooseDoctor(),
          SizedBox(height: 50.0,),
          // choose date
          datePicker,
          SizedBox(height: 50.0,),
          // choose time
          timePicker(),
          SizedBox(height: 50.0,),
          // submit
          submitBtn
        ],
      )
    ),
    );

    return Scaffold(
        appBar: AppBar(
          title: getTextWithFontAndSize(
            text: "Book an Appointment",
            color: Colors.white,
            fontSize: MEDIUM_LABEL_FONT_SIZE
          )
        ),
        body: BlocProvider(
          bloc: appointmentBloc,
          child: layout
        )
      );
  }
}




/*
Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                child: ListView(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  children: <Widget>[
                    Container(
                      child: Column(
                        children: <Widget>[
                          Text("Select Doctor"),
                          Center(
                            child: DropdownButton<User>(
                              value: selectedDoctor,
                              items: widget.doctors.map((doctor)=> 
                              DropdownMenuItem<User>(
                                child: Text("${doctor.firstName}"),
                                value: doctor,
                              )).toList(),
                              onChanged: (User selectedUser){
                                print("${selectedUser.firstName}");
                                setState(() {
                                    selectedDoctor = selectedUser;
                                          });
                              },
                            )
                          ),
                          Text("Pick Appointment Date"),
                          Text("${selectedDate.day}/${selectedDate.month}/${selectedDate.year}"),
                          RaisedButton(
                            child: Text("Pick Date"),
                            onPressed: () => _selectDate(context),
                          ),
                          Text("Pick Time Slot"),
                          Center(
                            child: DropdownButton<Slot>(
                              value: selectedSlot,
                              items: widget.slots.slot.map((slot)=>
                                DropdownMenuItem<Slot>(
                                  child: Text("${slot.startTime}"),
                                  value: slot,
                                )).toList(),
                              onChanged: (Slot slot){
                                print("${slot.startTime}");
                                setState(() {
                                      selectedSlot = slot;
                                });
                              },
                            )
                          ),
                          RaisedButton(
                            child: Text("Book Appointment"),
                            onPressed: (){
                              print("Appointment booking initiated");
                              print("Doctor Name: ${selectedDoctor.firstName}  ${selectedDoctor.lastName}");
                              print("Selected Slot: ${selectedSlot.startTime}");
                              Navigator.of(context)
                                .pushReplacement(
                                  MaterialPageRoute(builder: (context) => 
                                    CheckoutPage(
                                      selectedDoctor: selectedDoctor, 
                                      selectedDate: selectedDate, 
                                      selectedSlot: selectedSlot)));
                            },
                          )
                        ],
                      ),
                    )
                  ],
                )
              )
            ],
          ),
          */