import 'package:counter_clinic/pages/home_page_screen.dart';
import 'package:counter_clinic/pages/login_screen.dart';
import 'package:counter_clinic/pages/scan_qr_code_page.dart';
import 'package:counter_clinic/pages/sign_up_screen.dart';
import 'package:counter_clinic/utils/RequestHandler.dart';
import 'package:counter_clinic/utils/StorageHandler.dart';
import 'package:counter_clinic/utils/Utils.dart';
import 'package:flutter/material.dart';

class EntryScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Counter Clinic",
      theme: ThemeData(
        primarySwatch: Colors.green
      ),
      home: EntryScreenWidget(),
    );
  }
}

class EntryScreenWidget extends StatefulWidget {
  @override
  _EntryScreenWidgetState createState() => _EntryScreenWidgetState();
}

class _EntryScreenWidgetState extends State<EntryScreenWidget> {

  @override
  void initState() {
    super.initState();
  }

  // Future _checkLoginState() async {
  //   String tokensJson  = await getText(AUTH_TOKEN);
  //   if (tokensJson != null && tokensJson.isNotEmpty)
  //   {
  //     // user has logged in earlier
  //     print ("Token is present!");
  //     String accessToken  = getJsonValue(tokensJson, ACCESS_TOKEN);
      
  //     // check access token is valid or not
  //     RequestHandler handler = new RequestHandler();
  //     String authData =  await handler.authUser(accessToken);
  //     var negativeResponseCodes = ["400", "401", "403", "500"];

  //     if  (authData.isNotEmpty && !negativeResponseCodes.contains(authData))
  //     {
  //       print("Auth Data is not empty: $authData");
  //       isUserLoggedIn = true;
  //       _navigateToHomePage(context);
  //     }
  //   }
  // }

  _navigateToHomePage(BuildContext context) async
  {
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => new HomePageScreen()));
  }

  @override
  Widget build(BuildContext context) {

    Widget progressIndicator  =  Scaffold(
      body: Center(child: CircularProgressIndicator()),
    );

    Widget entryOptions = Scaffold(
        body: Stack(
          children: <Widget>[
            Positioned.fill(
              child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: ExactAssetImage('assets/gradient_green_background.jpg'),
                    fit: BoxFit.fill,
                    repeat: ImageRepeat.repeatY
                  )
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: BIG_BTN_WIDTH - 50,
                      height: 200.0,
                      margin: EdgeInsets.only(top: 10.0),
                      decoration: BoxDecoration(
                        image: DecorationImage(image: NetworkImage('https://i2.wp.com/www.bemyaficionado.com/wp-content/uploads/2017/04/bma-temp-logo.png'),fit: BoxFit.fill),
                      ),
                    ),
                    Container(
                      width: BIG_BTN_WIDTH + 100,
                      height: BIG_BTN_HEIGHT,
                      margin: EdgeInsets.only(top: 10.0),
                      alignment: Alignment.center,
                      child: getTextWithFontAndSize(text: "Counter Clinic", fontSize: 42.0, color: Colors.white),
                    ),
                    Container(
                      width: BIG_BTN_WIDTH,
                      height: BIG_BTN_HEIGHT,
                      margin: EdgeInsets.only(top: 10.0),
                      child: RaisedButton(
                        child: getTextWithFontAndSize(text: "Login", fontSize: 28.0, color: Colors.white),
                        onPressed: () { 
                          Navigator.push(context, MaterialPageRoute(builder:  (context) => new LoginScreen()));
                        },
                        color: Colors.transparent,
                        shape: Border.all(color: Colors.white),
                      ),
                    ),
                    Container(
                      width: BIG_BTN_WIDTH,
                      height: BIG_BTN_HEIGHT,
                      margin: EdgeInsets.only(top: 10.0),
                      child: RaisedButton(
                        child: getTextWithFontAndSize(text: "Sign Up", fontSize: 28.0, color: Colors.white),
                        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => new SignUpScreen())),
                        color: Colors.transparent,
                        shape: Border.all(color: Colors.white),
                      ),
                    ),
                    Container(
                      width: BIG_BTN_WIDTH,
                      height: BIG_BTN_HEIGHT,
                      margin: EdgeInsets.only(top: 10.0),
                      child: RaisedButton(
                        child: getTextWithFontAndSize(text: "Login as Guest", fontSize: 28.0, color: Colors.white),
                        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => new ScanQrCodePage())),
                        color: Colors.transparent,
                        shape: Border.all(color: Colors.white),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        )
      );

    // _checkLoginState().then((_){
    //   return entryOptions;
    // });

    return entryOptions;
  }
}