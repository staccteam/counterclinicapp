import 'package:counter_clinic/pages/appointment_status_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class OfflineScanScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    TextEditingController appointmentNumberController = new TextEditingController();

    Widget _appointmentNumberInput()
    {
      return Column(
        children: <Widget>[
          TextField(
            controller: appointmentNumberController,
            keyboardType: TextInputType.numberWithOptions(
              decimal: false,
              signed: true
            ),
          ),

          RaisedButton.icon(
            icon: Icon(Icons.navigate_next),
            label: Text("Submit"),
            onPressed: () async {
              print("Submitted. Value: ${appointmentNumberController.text}");     
              Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => 
                              new AppointmentStatusPage(
                                appointmentNumber: int.tryParse(appointmentNumberController.text))));
              
              return;       
            },
          )


        ],
      );
    }

    Widget layout()
    {
      return Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              _appointmentNumberInput()
            ],
          )
        ],
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Offline Scan",
          style: TextStyle(
          fontSize: 26.0,
          color: Colors.white
          ),
        ),
      ),
      body: layout(),
    );
  }
}