import 'dart:convert';

import 'package:counter_clinic/pages/entry_screen.dart';
import 'package:counter_clinic/pages/login_screen.dart';
import 'package:counter_clinic/partials/view_components.dart';
import 'package:counter_clinic/utils/RequestHandler.dart';
import 'package:counter_clinic/utils/Utils.dart';
import 'package:counter_clinic/models/user.dart';
import 'package:flutter/material.dart';

class SignUpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return getAppTheme(
      homeWidget: new SignUpPage(title: "Sign Up!")
    );
  }
}

class SignUpPage extends StatefulWidget {

  String title;

  SignUpPage({Key key, @required this.title}) : super(key:  key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class SignUpState {
  bool isLoading;
  bool isSuccess;

  SignUpState({Key key, this.isLoading, this.isSuccess});
}

class _SignUpPageState extends State<SignUpPage> {

  var _signUpState = SignUpState(isLoading: false, isSuccess: false);

  var usernameController = new TextEditingController();
  var passwordController = TextEditingController();
  var confirmPasswordController = TextEditingController();
  var firstNameController = TextEditingController();
  var lastNameController = TextEditingController();
  var emailController = TextEditingController();

  Widget _customTextField({
    @required TextEditingController textEditingController,
    @required String labelText,
    TextStyle textStyle = const TextStyle(color: Colors.white),
    TextStyle labelStyle = const TextStyle(color: Colors.white, fontFamily: 'Roboto', fontSize: 22.0),
    bool isObscureText = false
    }) {
    return Container(
      margin: EdgeInsets.only(top: 15.0),
      child: TextField(
        controller: textEditingController,
        obscureText: isObscureText,
        style: textStyle,
        decoration: InputDecoration(
          labelText: labelText,
          labelStyle: labelStyle,
        ),
      ),
    );
  }

  _handleSignUp({BuildContext  context}) async {
    print("Signup Handled!");
    showSnackBarWithMessage(context: context,  msg: "Please wait...", backgroundColor: Colors.orange);

    var user = User();
    user.firstName = firstNameController.text;
    user.lastName = lastNameController.text;
    user.username  =  usernameController.text;
    user.password  =  passwordController.text;
    user.email = emailController.text;

    user.roles = [
      Roles.fromJson({
        "name": "PATIENT",
        "permissions": [{
          "name":"CREATE_NEW_APPOINTMENT"
        }]
      })
    ];
    
    RequestHandler handler  = new RequestHandler();
    String response = await handler.registerNewUser(user);

    var savedUser = User.fromJson(json.decode(response));
    if  (savedUser.id  != null)
    {
      showSnackBarWithMessage(context: context, msg: "Sign Up Successful!", backgroundColor: Colors.green);
      showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext build)
        {
          return AlertDialog(
                  title: Text("You have been registered successfully."),
                );
        }
      );
      
      _navigateToLoginScreen(context, "Registered Successfully!");
      return;
    }
    
    showSnackBarWithMessage(context: context, msg: "There was some problem.", backgroundColor: Colors.red);
 
    print("User registered: $response");
  }

  void _navigateToLoginScreen(BuildContext context, String msg)
  {
    Navigator.push(context, MaterialPageRoute(builder: (context) => new LoginScreen(msg: msg)));
  }

  void _navigateToEntryScreen(BuildContext context, String msg)
  {
    Navigator.push(context, MaterialPageRoute(builder: (context) => new EntryScreen()));
  }

  @override
  Widget build(BuildContext context) {

    Widget loading = Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );

    Widget signupForm = Scaffold(
      // appBar: AppBar(
      //   title: Text(widget.title),
      // ),
      body: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Container(
              alignment: Alignment.center,
              height: 100.0,
              width: 100.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: ExactAssetImage('assets/gradient_green_background.jpg'),
                  fit: BoxFit.fill,
                  repeat: ImageRepeat.repeatY
                )
              ),
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Container(
                  margin: EdgeInsets.only(top: 20.0),
                  child: ListView(
                    children: <Widget>[
                      // Container(
                      //   height: 200.0,
                      //   width: 200.0,
                      //   margin: EdgeInsets.only(top: 10.0),
                      //   decoration: BoxDecoration(
                      //     image: DecorationImage(image: NetworkImage('https://i2.wp.com/www.bemyaficionado.com/wp-content/uploads/2017/04/bma-temp-logo.png'),fit: BoxFit.fill),
                      //   ),
                      // ),
                      Container(
                        margin: EdgeInsets.only(top: 10.0),
                        alignment: Alignment.center,
                        child: getTextWithFontAndSize(text: "Sign Up Form", fontSize: 42.0, color: Colors.white),
                      ),
                      _customTextField(
                        textEditingController: firstNameController,
                        labelText: "First Name"
                      ),
                      _customTextField(
                        textEditingController: lastNameController,
                        labelText: "Last Name"
                      ),
                      _customTextField(
                        textEditingController: emailController,
                        labelText: "E-Mail"
                      ),
                      _customTextField(
                        textEditingController: usernameController,
                        labelText: "Username"
                      ),
                      _customTextField(
                        textEditingController: passwordController,
                        isObscureText: true,
                        labelText: "Password"
                      ),
                      _customTextField(
                        textEditingController: confirmPasswordController,
                        isObscureText: true,
                        labelText: "Confirm Password"
                      ),
                      Builder(
                        builder: (BuildContext context){
                          return Container(
                            height: BIG_BTN_HEIGHT,
                            width: BIG_BTN_WIDTH,
                            margin: EdgeInsets.only(top: 15.0),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.white, width: 2.0),
                            ),
                            child: RaisedButton(
                              color: Colors.transparent,
                              child: getTextWithFontAndSize(text: "Sign Up", fontSize: 28.0, color: Colors.white),
                              onPressed: () =>  _handleSignUp(context: context),
                            ),
                          );
                        }
                      )
                    ],
                  ),
                ),
              )
            ),
          )
        ],
      )
    );
    
    return signupForm;
  }
}