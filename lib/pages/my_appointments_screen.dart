import 'package:counter_clinic/blocs/appointment_bloc.dart';
import 'package:counter_clinic/models/online_appointment.dart';
import 'package:counter_clinic/pages/create_appointment_screen.dart';
import 'package:counter_clinic/partials/view_components.dart';
import 'package:counter_clinic/widgets/appointment_row.dart';
import 'package:counter_clinic/widgets/bloc_provider.dart';
import 'package:flutter/material.dart';

class MyAppointments extends StatelessWidget {

  final AppointmentBloc appointmentBloc = new AppointmentBloc();

  MyAppointments()
  {
    appointmentBloc.fetchOnlineAppointments();
  }

  Widget _buildTimeline() {
  return new Positioned(
    top: 0.0,
    bottom: 0.0,
    left: 32.0,
    child: new Container(
      width: 1.0,
      color: Colors.grey[300],
    ),
  );
}

  Widget listOfAppointments() {
    return StreamBuilder<List<OnlineAppointment>>(
      stream: appointmentBloc.outputOnlineAppointment,
      builder: (context, snapshot){
        if (snapshot.hasData)
        {
          // no appointments
          if (snapshot.data.isEmpty)
          {
            return Center(
              child: Text("There are no appointments to show.", style: TextStyle(
                fontSize: 26.0,
                fontFamily: "Roboto"
              ),),
            );
          }
          // data found
          return ListView(
            children: snapshot.data.map((onlineAppointment)=> new AppointmentRow(bookedAppointment: onlineAppointment,)).toList()
          );
        }

        // error
        if (snapshot.hasError)
        {
          return Center(
            child: Text("We are experiencing some technical difficulties. Please check later.",
              style: TextStyle(
                fontSize: 26.0,
                color: Colors.red,
                fontFamily: "Roboto"
              ),
            ),
          );
        }
        
        // loader
        return Center(
          child: CircularProgressIndicator(),
        );
      }
    );
  }

  Widget layout()
  {
    return Stack(
      children: <Widget>[
        listOfAppointments(),
        _buildTimeline()
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
        appBar: AppBar(
          title: Text("My Appointments"),
        ),
        drawer: getDrawer(context),
        body: BlocProvider(
          bloc: appointmentBloc,
          child: layout()
        ),
        floatingActionButton: FloatingActionButton(
            elevation: 0.5,
            child: Icon(Icons.add),
            onPressed: () async {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CreateAppointment()));
            }));
  }
}
