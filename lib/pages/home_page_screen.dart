import 'package:counter_clinic/pages/Screens.dart';
import 'package:counter_clinic/pages/appointment_status_screen.dart';
import 'package:counter_clinic/partials/view_components.dart';
import 'package:counter_clinic/utils/JwtParser.dart';
import 'package:counter_clinic/utils/Utils.dart';
import 'package:counter_clinic/models/AppointmentPayload.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'dart:async';
import 'dart:convert';
import 'package:counter_clinic/utils/QRScanner.dart';
import 'package:counter_clinic/utils/RequestHandler.dart';
import 'package:counter_clinic/models/AppointmentVO.dart';

class HomePageScreen extends StatelessWidget {

  HomePageScreen() {
    currentDrawerState = Screens.MAIN;
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return getAppTheme(
      title: 'Flutter Demo',
      homeWidget:  new MyHomePage(title: 'Counter Clinic'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  AppointmentVO _appointmentVO;
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  
  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      appBar: new AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: getTextWithFontAndSize(
          text: widget.title,
          fontSize: MEDIUM_LABEL_FONT_SIZE,
          color: Colors.white
        )
      ),
      drawer: getDrawer(context),
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/blue_grafica_design_background.jpg'),
                fit: BoxFit.fill
              )
            ),
          ),
          new Center(
            child: new Column(

              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.network(LOGO_DEFAULT_PATH, width: 200.0,),
                // new Container(
                //   child: new Image.asset("assets/qr-code-placeholder.jpeg"),
                // ),
                new Text(
                  'Press to Scan and Get your Appointment details.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 24.0
                  ),
                ),
                new RaisedButton(
                  child: getTextWithFontAndSize(
                    text: "SCAN QR CODE",
                    fontSize: SMALL_BTN_HEIGHT-15,
                    color: Colors.white
                  ),
                  color: Colors.green,
                  elevation: 4.0,
                  splashColor: Colors.deepPurple,
                  onPressed: () {
                    QRScanner scanner = new QRScanner();
                    var barcodeProbableValue = scanner.scan();
                    barcodeProbableValue.then((String appointmentIdJson){
                      print("Appointment Json: " + appointmentIdJson);
                      Map<String, dynamic> appointmentObj = jsonDecode(appointmentIdJson);
                      RequestHandler handler = new RequestHandler();

                      Future<String> futureToken  = handler.fetchAppointmentToken(appointmentObj['appointmentId']);
                      futureToken.then((String futureTokenResponse){
                        Map<String, dynamic> appointmentToken = jsonDecode(futureTokenResponse);
                        String jwtToken  = appointmentToken['data'];
                        print("JWT Token:  "  + jwtToken);

                        JwtParser jwtParser =  new JwtParser(jwtToken);
                        print(jwtParser.payload);
                        AppointmentPayload appointmentPayload =  AppointmentPayload.fromJson(jwtParser.payload);
                        if  (appointmentPayload  == null) {
                          print("Appointment Payload is Null");
                        }  else  {
                          print("Appointment Payload Time:  ${appointmentPayload.aud}");
                        }
                        Navigator.pushReplacement(context, MaterialPageRoute(builder:  (context) => AppointmentStatusScreen(jwtToken: jwtToken, hisAppointmentStatus: appointmentPayload)));
                      });
                    });

                    //Navigator.push(context, MaterialPageRoute(builder: (context)  => ScanScreen()));

                  },

                ),
              ],
            ),
          ),
        ],
      )
      
      // floatingActionButton: new FloatingActionButton(
      //   onPressed: _incrementCounter,
      //   tooltip: 'Increment',
      //   child: new Icon(Icons.add),
      // ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }


}