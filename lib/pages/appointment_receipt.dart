import 'package:counter_clinic/blocs/create_appointment_bloc.dart';
import 'package:counter_clinic/pages/my_appointments_screen.dart';
import 'package:counter_clinic/utils/Utils.dart';
import 'package:flutter/material.dart';

class AppointmentReceiptPage extends StatelessWidget {
  final AppointmentPageViewData viewData;

  AppointmentReceiptPage({Key key, @required this.viewData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    var labelStyle = Theme.of(context).textTheme.body1;
    var textStyle = Theme.of(context).textTheme.body2;

    Widget _rowWithTwoColumn({@required String column1, @required String column2})
    {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(column1,
                textAlign: TextAlign.start,
                style: TextStyle(
                ),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(column2, 
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.body2,
              ),
            ),
          )
        ],
      );
    }

    Widget receiptData()
    {
      return Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  _rowWithTwoColumn(
                    column1: "Appointed Doctor: ",
                    column2: "${this.viewData.selectedDoctor.firstName} ${this.viewData.selectedDoctor.lastName}"
                  ),
                  _rowWithTwoColumn(
                    column1: "Appointment Date: ",
                    column2: "${formatDate(viewData.selectedDate)}"
                  ),
                  _rowWithTwoColumn(
                    column1: "Appointment Time: ",
                    column2: "${viewData.selectedTimeSlot.startTime}"
                  ),
                  _rowWithTwoColumn(
                    column1: "Total Fees: ",
                    column2: "10 AED"
                  )
                ]
              );
    } 

    Widget actionButton = ButtonTheme.bar(
            child: ButtonBar(
              children: <Widget>[
                FlatButton(
                  child: const Text("Done"),
                  onPressed: (){
                    showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (context){
                        return AlertDialog(
                          title: Text("Thanks for the booking. An email will reach you shortly."),
                          actions: <Widget>[
                            FlatButton(
                              child: Text("OK"),
                              onPressed: ()=>Navigator.pop(context),
                            )
                          ],
                        );
                      },
                    );
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MyAppointments()));
                  },
                )
              ],
            ),
          );

    Widget card = Card(
      child: Column(
        children: <Widget>[
          receiptData(),
          actionButton
        ],
      )
    );


    Widget layout = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        card
      ],
    );
    
    return Scaffold(
      appBar: AppBar(
        title: getTextWithFontAndSize(
          text: "Appointment Receipt",
        ),
      ),
      body: layout,
    );
  }
}