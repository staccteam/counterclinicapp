import 'package:counter_clinic/blocs/appointment_bloc.dart';
import 'package:counter_clinic/models/appointment.dart';
import 'package:counter_clinic/models/online_appointment.dart';
import 'package:counter_clinic/utils/Utils.dart';
import 'package:counter_clinic/widgets/appointment_row.dart';
import 'package:counter_clinic/widgets/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DoctorHomeScreen extends StatelessWidget {

  final AppointmentBloc appointmentBloc = new AppointmentBloc();

  @override
  Widget build(BuildContext context) {
    appointmentBloc.setIndex(0);

    Widget _bottomNavigationBar()
    {
      return StreamBuilder<int>(
        stream: appointmentBloc.outputTabIndex,
        initialData: 0,
        builder: (context, snapshot){
          return BottomNavigationBar(
            currentIndex: snapshot.data,
            onTap: (tabIndex){
              appointmentBloc.setIndex(tabIndex);
            },
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.directions_walk),
                title: Text("Walk-In"),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.signal_cellular_connected_no_internet_4_bar),
                title: Text("Online Appointments")
              )
            ],
          );
        },
      );
    }

    Widget _appbar()
    {
      return AppBar(
        title: Text("Doctor Summary",
          style: TextStyle(
            fontSize: 26.0,
            fontWeight: FontWeight.w700,
            color: Colors.white
          ),
        ),
      );
    }

    Widget _statusRow(String key, String val)
    {
      
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(key,
              style: Theme.of(context).textTheme.body2,
            ),
            Text(val,
              style: Theme.of(context).textTheme.body2,
            )
          ],
        ),
      );
    }

    Widget _showDoctorStats(AppointmentQueueStatus  data)
    {
      int _calculateApproxCheckupTime(){
        return data.avgCheckupTime * (data.queueSize - data.currQueueCount);
      }
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _statusRow("Queue Size: ", "${data.queueSize}"), 
          Divider(),
          _statusRow("Current Patient Count: ", "${data.currQueueCount}"),
          Divider(),
          _statusRow("Average Session Time: ", "${Duration(seconds: data.avgCheckupTime).inMinutes} min"),
          Divider(),
          _statusRow("Approx. Total Checkup Time: ", "${Duration(seconds: _calculateApproxCheckupTime()).inMinutes} min"),
          Divider(),
          _statusRow("Last Patient In-Time: ", "${data.currQueueCount == 0 ? '--:--' : new DateFormat('hh:mm a').format(new DateTime.fromMillisecondsSinceEpoch(data.patientInTimeStamp)) }"),
        ],
      );
    }

    Widget _doctorStatusWithCard(Widget doctorStatus)
    {
      Widget _refreshBtn()
      {
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            RaisedButton(
              child: Icon(Icons.refresh),
              onPressed: (){
                appointmentBloc.fetchDoctorQueueSummary();
              },
            )
          ],
        );
      }

      return Card(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ListTile(
              // leading: Icon(Icons.album),
              title: Text('Doctor Queue Status',
                style: Theme.of(context).textTheme.headline,
              ),
              subtitle: Text('Doctor queue status will automatically update every 20 seconds.'),
            ),
            Divider(),
            doctorStatus,
            _refreshBtn()
          ])
      );
    }

    Widget _walkInLayout()
    {
      appointmentBloc.fetchDoctorQueueSummary();
      return StreamBuilder<AppointmentQueueStatus>(
        stream: appointmentBloc.outputAppointmentQueueStatus,
        builder: (context, snapshot){
          if(snapshot.hasData)
          {
            return Stack(
              children: <Widget>[
                _doctorStatusWithCard(_showDoctorStats(snapshot.data)),
                
              ],
            );
          }
          return  Center(
            child: CircularProgressIndicator(),
          );
        },
      );
    }

    Widget _listOfOnlineAppointments(List<OnlineAppointment> onlineAppointments)
    {
      Widget _refreshBtn()
      {
        return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            RaisedButton(
              child: Icon(Icons.refresh),
              onPressed: (){
                appointmentBloc.fetchOnlineAppointments();
              },
            )
          ],
        );
      }

      return Column(
        children: [
          Column(
            children: onlineAppointments.map((onlineAppointment)=> new AppointmentRow(bookedAppointment: onlineAppointment,)).toList()
          ),
          _refreshBtn()
        ]
      );
    }

    Widget _onlineAppointmentLayout()
    {
      appointmentBloc.fetchOnlineAppointments();
      return StreamBuilder<List<OnlineAppointment>>(
        stream: appointmentBloc.outputOnlineAppointment,
        builder: (context, snapshot){
          if (snapshot.hasData)
          {
            return Stack(
              children: <Widget>[
                _listOfOnlineAppointments(snapshot.data)
              ],
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      );
      
    }

    Widget selectLayout()
    {
      return StreamBuilder<int>(
        stream: appointmentBloc.outputTabIndex,
        builder: (context, snapshot){
          if (snapshot.hasData && snapshot.data == 1)
          {
            return _onlineAppointmentLayout();
          }
          return _walkInLayout();
        },
      );
    }

    return Scaffold(
      appBar: _appbar(),
      bottomNavigationBar: _bottomNavigationBar(),
      body: BlocProvider(
        bloc: appointmentBloc,
        child: selectLayout()
      ),
    );
  }
}  