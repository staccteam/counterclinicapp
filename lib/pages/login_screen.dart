import 'package:counter_clinic/pages/doctor_home_screen.dart';
import 'package:counter_clinic/pages/home_page_screen.dart';
import 'package:counter_clinic/partials/view_components.dart';
import 'package:counter_clinic/utils/RequestHandler.dart';
import 'package:counter_clinic/utils/StorageHandler.dart';
import 'package:counter_clinic/utils/Utils.dart';
import 'package:counter_clinic/models/user.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

class LoginScreen extends StatelessWidget {

  String msg = "";

  LoginScreen({this.msg});

  @override
  Widget build(BuildContext context) {

    return getAppTheme(
      title: "Login Screen",
      primarySwatch: Colors.green,
      homeWidget: new LoginScreenPage(title: "Login Page", msg: msg),
    );
  }
}

class LoginScreenPage extends StatefulWidget {
  String title;
  String msg;

  LoginScreenPage({Key key, this.title, this.msg}) : super(key:key);

  @override
  _LoginScreenPageState createState() => _LoginScreenPageState();
}

class LoginState {
  bool isLoading;
  bool isAuthenticated;

  LoginState({Key key, this.isLoading, this.isAuthenticated});
}

class _LoginScreenPageState extends State<LoginScreenPage> {

  var usernameController = TextEditingController();
  var passwordController = TextEditingController();

  _handleLogin({BuildContext context}) async {
    // if username or password field is empty
    if (usernameController.text.isEmpty ||  passwordController.text.isEmpty) {
      showSnackBarWithMessage(context: context, msg: "Username/Password cannot be empty", backgroundColor: Colors.red);
      return;
    }

    // show info message, please wait..
    showSnackBarWithMessage(context: context, msg: "Please wait...", backgroundColor: Colors.orange);
    // call login service and pass the user sign-in credentials
    RequestHandler handler  = new RequestHandler();
    String response = await handler.loginUser(usernameController.text, passwordController.text);
    var negativeResponseCodes = ["400", "401", "403", "500"];
    if (negativeResponseCodes.contains(response))
    {
      showSnackBarWithMessage(context: context, msg: "Invalid Username/Password", backgroundColor: Colors.red);
      usernameController.text = "";
      passwordController.text = "";
      return;
    }

    print("Response from login action: $response");
    Map<String, dynamic> loginResponse = json.decode(response);
    if (loginResponse.isEmpty)
    {
      showSnackBarWithMessage(context: context, msg: "Invalid Username/Password", backgroundColor: Colors.red);
      return;
    }
    // On positive response, decode auth tokens and store it      
    if (! loginResponse.containsKey(ACCESS_TOKEN))
    {
      showSnackBarWithMessage(context: context, msg: "Invalid Username/Password", backgroundColor: Colors.red);
      return;
    }

    showSnackBarWithMessage(context: context, msg: "Login Successful!", backgroundColor: Colors.green);
    saveText(AUTH_TOKEN, response);

    // fetch access token from auth token and give call to auth service for user's auth data
    String authResponse = await handler.authUser(loginResponse[ACCESS_TOKEN]);
    print("User auth response:  $authResponse");
    
    isUserLoggedIn = true;

    User user = User.fromJson(json.decode(authResponse));
    user.roles.forEach((r){
      if  (r.name.contains("DOCTOR"))
      {
        print("Doctor Logged In!");
        _navigateToDoctorDashboard(context: context);
      }

      if (r.name.contains("PATIENT"))
      {
        print("Patient Logged  In!");
        _navigateToDashboard(context:  context);
      }
    });
    // JwtParser parser = new JwtParser(loginResponse["accessToken"]);
    // print("Login Response Payload: ${parser.payload}");
    
  }

  void _navigateToDashboard({BuildContext context}) {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => new HomePageScreen()));
  }

  void _navigateToDoctorDashboard({BuildContext context}) {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => new DoctorHomeScreen()));
  }

  @override
  initState() {
    super.initState();
  }

    
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text(widget.title),
      // ),
      body: Stack(
        children: <Widget>[
          Positioned.fill(
            child: Container(
              alignment: Alignment.center,
              height: 100.0,
              width: 100.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: ExactAssetImage('assets/gradient_green_background.jpg'),
                  fit: BoxFit.fill,
                  repeat: ImageRepeat.repeatY
                )
              ),
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Container(
                  margin: EdgeInsets.only(top: 20.0),
                  child: ListView(
                    children: <Widget>[
                      Container(
                        height: 200.0,
                        width: 200.0,
                        margin: EdgeInsets.only(top: 10.0),
                        decoration: BoxDecoration(
                          image: DecorationImage(image: NetworkImage('https://i2.wp.com/www.bemyaficionado.com/wp-content/uploads/2017/04/bma-temp-logo.png'),fit: BoxFit.fill),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 10.0),
                        alignment: Alignment.center,
                        child: getTextWithFontAndSize(text: "Counter Clinic", fontSize: 42.0, color: Colors.white),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 15.0),
                        child: TextField(
                          controller: usernameController,
                          style: TextStyle(
                            color: Colors.white
                          ),
                          decoration: InputDecoration(
                            labelText: "Username/Email",
                            labelStyle: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Roboto',
                              fontSize: 22.0
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 15.0),
                        child: TextField(
                          controller: passwordController,
                          obscureText: true,
                          style: TextStyle(
                            color: Colors.white,
                          ),
                          decoration: InputDecoration(
                            labelText: "Password",
                            labelStyle: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Roboto',
                              fontSize: 22.0
                            ),
                          )
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          alignment: Alignment.centerRight,
                          child: InkWell(
                            child: getTextWithFontAndSize(text: "Forgot Password?", color: Colors.white),
                          ),
                        ),
                      ),
                      Builder(
                        builder: (BuildContext context){
                          return Container(
                            height: BIG_BTN_HEIGHT,
                            width: BIG_BTN_WIDTH,
                            margin: EdgeInsets.only(top: 15.0),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.white, width: 2.0),
                            ),
                            child: RaisedButton(
                              color: Colors.transparent,
                              child: getTextWithFontAndSize(text: "Login", fontSize: 28.0, color: Colors.white),
                              onPressed: () =>  _handleLogin(context: context),
                            ),
                          );
                        }
                      )
                    ],
                  ),
                ),
              )
            ),
          )
        ],
      )
    );
  }
}