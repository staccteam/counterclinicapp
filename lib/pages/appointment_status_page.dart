import 'package:counter_clinic/blocs/appointment_status_bloc.dart';
import 'package:counter_clinic/partials/view_components.dart';
import 'package:counter_clinic/widgets/bloc_provider.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class AppointmentStatusPage extends StatelessWidget {

  final int appointmentNumber;

  AppointmentStatusPage({Key key, @required this.appointmentNumber}): super(key: key);

  

  

  @override
  Widget build(BuildContext context) {
    AppointmentStatusBloc appointmentStatusBloc = new AppointmentStatusBloc(appointmentNumber);

    Widget _simpleTimer()
    {
      return StreamBuilder<AppointmentStatusPageViewData>(
        stream: appointmentStatusBloc.outputStream,
        builder: (context, snapshot)
        {
          ///  creates a text with display1 style
          Widget _statusText(String text)
          {
            return Text(text,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.display1,
            );
          }

          if (snapshot.hasData 
            && snapshot.data.appointmentStatus != null 
            && snapshot.data.appointmentStatus.isNotEmpty)
          {
            if (snapshot.data.showStatus)
              return _statusText(snapshot.data.appointmentStatus); // display status message
            
            String displayTime = new DateFormat('hh:mm:ss').format(
              new DateTime.fromMillisecondsSinceEpoch(
                new DateTime.now().millisecondsSinceEpoch
                  + Duration(seconds: snapshot.data.timeRemainingInSeconds).inMilliseconds));

            return Column(
              children: <Widget>[
                _statusText("Appointment Time"),
                _statusText(snapshot.data.appointmentStatus),  //  display appointment time
                Text("$displayTime") // dislpay time remaining
              ],
            );
          }

          return Center(
            child:  LinearProgressIndicator(),
          );
        },
      );
    }

    Widget _rowWithTwoColumn({@required String column1, @required String column2})
    {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(column1,
                textAlign: TextAlign.start,
                style: TextStyle(
                ),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(column2, 
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.body2,
              ),
            ),
          )
        ],
      );
    }

    Widget _buildAppointmentStatus()
    {
      return StreamBuilder<AppointmentStatusPageViewData>(
        stream: appointmentStatusBloc.outputStream,
        builder: (context, snapshot){
          if (!snapshot.hasData || snapshot.data.appointmentQueueStatus == null || snapshot.data.appointment == null)
          {
            return Center(
              child: CircularProgressIndicator(),
            );
          }

          if (snapshot.hasError)
          {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("There is no appointment with your number.")
              ],
            );
          }

          if (snapshot.hasData 
            && snapshot.data.appointment != null 
            && snapshot.data.appointmentQueueStatus != null)
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  _rowWithTwoColumn(
                    column1:  "Doctor Name: ", 
                    column2: "${snapshot.data.appointment.doctorName}"
                  ),
                  Divider(),
                  _rowWithTwoColumn(
                    column1:  "Total Patients in the queue: ", 
                    column2: "${snapshot.data.appointmentQueueStatus.queueSize}"
                  ),
                  Divider(),
                  _rowWithTwoColumn(
                    column1: "Current Patient Number: ",
                    column2: "${snapshot.data.appointmentQueueStatus.currQueueCount}"
                  ),
                  Divider(),
                  _rowWithTwoColumn(
                    column1: "Your Number: ",
                    column2: "${snapshot.data.appointment.appointmentNumber}"
                  ),
                  Divider(),
                  _rowWithTwoColumn(
                    column1: "Break Time: ",
                    column2: "${Duration(milliseconds: snapshot.data.appointmentQueueStatus.extendedTime).inMinutes}"
                  ),
                  Divider(),
                  _rowWithTwoColumn(
                    column1: "Doctor Status: ",
                    column2: "${snapshot.data.appointmentQueueStatus.doctorStatus}"
                  ),
                  Divider(),
                ],
              ),
            ),
          );
        },
      );
    }

    Widget layout() {
      return Stack(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              _buildAppointmentStatus(),
              // BlocProvider(
              //   bloc: AppointmentStatusBloc(appointmentNumber),
              //   child: _simpleTimer(),
              // )
              _simpleTimer()
            ],
          )
        ],
      );
    }

    return BlocProvider(
      bloc: appointmentStatusBloc,
      child: Scaffold(
      appBar: AppBar(
          title: Text("Appointment Status", 
            style: TextStyle(
              fontSize: 26.0,
              color: Colors.white
            ),
          ),
        ),
        body: layout() 
      ),
    );
  }

  
}