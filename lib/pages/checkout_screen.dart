import 'package:counter_clinic/utils/Utils.dart';
import 'package:counter_clinic/models/slots.dart';
import 'package:counter_clinic/models/user.dart';
import 'package:flutter/material.dart';

class CheckoutPage extends StatelessWidget {

  User selectedDoctor;
  Slot selectedSlot;
  DateTime selectedDate;

  CheckoutPage({@required this.selectedDoctor, @required this.selectedSlot, @required this.selectedDate});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: getTextWithFontAndSize(
          text: "Book an Appointment",
          color: Colors.white,
          fontSize: MEDIUM_LABEL_FONT_SIZE
        ),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Appointment Date: ${selectedDate.month}/${selectedDate.day}/${selectedDate.year}"),
            Text("Appointment Time: ${selectedSlot.startTime}"),
            Text("Appointed Doctor: ${selectedDoctor.firstName} ${selectedDoctor.lastName}"),
            RaisedButton(
              child: Text("Checkout"),
              onPressed: (){
                print("Appointment Booked Successfully!");
              },
            )
          ],
        ),
      ),
    );
  }
}