import 'dart:async';

import 'package:counter_clinic/blocs/timer_bloc.dart';
import 'package:counter_clinic/widgets/bloc_provider.dart';
import 'package:flutter/material.dart';

class ApproxTimeCalculator extends StatelessWidget {

  final TimerBloc timerBloc = new TimerBloc();

  Widget layout()
  {
    return StreamBuilder<String>(
      stream: timerBloc.outputStream,
      builder: (context, snapshot){
        if  (snapshot.hasData)
        {
          
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: timerBloc,
      child: layout(),
    );
  }
}