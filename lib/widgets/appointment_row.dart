import 'package:counter_clinic/blocs/create_appointment_bloc.dart';
import 'package:counter_clinic/models/online_appointment.dart';
import 'package:counter_clinic/utils/Utils.dart';
import 'package:flutter/material.dart';

class AppointmentRow extends StatefulWidget {
  final OnlineAppointment bookedAppointment;
  final double dotSize = 12.0;

  const AppointmentRow({Key key, this.bookedAppointment}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new AppointmentRowState();
  }
}

class AppointmentRowState extends State<AppointmentRow> {
  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: new Row(
        children: <Widget>[
          new Padding(
            padding: new EdgeInsets.symmetric(horizontal: 32.0 - widget.dotSize / 2),
            child: new Container(
              height: widget.dotSize,
              width: widget.dotSize,
              decoration: new BoxDecoration(shape: BoxShape.circle, color: Colors.cyan),
            ),
          ),
          new Expanded(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new Text(
                  "Doctor: ${widget.bookedAppointment.doctor.firstName} ${widget.bookedAppointment.doctor.lastName}",
                  style: new TextStyle(fontSize: 18.0),
                ),
                new Text(
                  "Patient: ${widget.bookedAppointment.patient.firstName} ${widget.bookedAppointment.patient.lastName}",
                  style: new TextStyle(fontSize: 18.0),
                ),
                new Text(
                  widget.bookedAppointment.date,
                  style: new TextStyle(fontSize: 12.0, color: Colors.grey),
                )
              ],
            ),
          ),
          
          new Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: new Text(
              widget.bookedAppointment.slot.startTime,
              style: new TextStyle(fontSize: 12.0, color: Colors.grey),
            ),
          ),
          Divider()
        ],
      ),
    );
  }
}