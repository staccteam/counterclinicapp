import 'dart:async';

import 'package:counter_clinic/api/users_api.dart';
import 'package:counter_clinic/models/user.dart';
import 'package:counter_clinic/widgets/bloc_provider.dart';

class UserBloc extends BlocBase
{

  StreamController<List<User>> _userStreamController = StreamController<List<User>>();
  Sink<List<User>> get inputUsers => _userStreamController.sink;
  Stream<List<User>> get outputUsers => _userStreamController.stream;

  void fetchUsers() async
  {
    UsersApi api = new UsersApi();
    inputUsers.add(await api.fetchAllDoctors());
  }

  @override
  void dispose() {
    _userStreamController.close();
  }

}