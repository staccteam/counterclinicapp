import 'dart:async';

import 'package:counter_clinic/api/site_options_api.dart';
import 'package:counter_clinic/models/site_options.dart';
import 'package:counter_clinic/widgets/bloc_provider.dart';

class SiteOptionBloc extends BlocBase
{

  final SiteOptionsApi _siteOptionsApi = new SiteOptionsApi();

  StreamController<SiteOptions> _siteOptionsController = new StreamController<SiteOptions>.broadcast();
  Stream<SiteOptions> get siteOptionsStream => _siteOptionsController.stream;
  Sink<SiteOptions> get _siteOptionsSink => _siteOptionsController.sink;

  void fetchAllSiteOptions() async
  {
    Future<SiteOptions> siteOptions = _siteOptionsApi.fetchAllSiteOptions();
    _siteOptionsSink.add(await siteOptions);
  }

  @override
  void dispose() {
    _siteOptionsController.close();
  }

}