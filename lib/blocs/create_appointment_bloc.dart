import 'dart:async';

import 'package:counter_clinic/api/appointment_api.dart';
import 'package:counter_clinic/api/users_api.dart';
import 'package:counter_clinic/models/slots.dart';
import 'package:counter_clinic/models/user.dart';
import 'package:counter_clinic/widgets/bloc_provider.dart';

class AppointmentPageViewData
{
  List<User> doctors;
  Slots timeSlots;
  
  User selectedDoctor;
  DateTime selectedDate;
  Slot selectedTimeSlot;
  bool loader;
}

class CreateAppointmentBloc extends BlocBase
{

  final AppointmentPageViewData viewData = AppointmentPageViewData();
  final AppointmentApi api = AppointmentApi();

  StreamController<AppointmentPageViewData> _mainStreamController = StreamController<AppointmentPageViewData>.broadcast();
  Sink<AppointmentPageViewData> get _sinkMainStreamData => _mainStreamController.sink;
  Stream<AppointmentPageViewData> get  outputMainStream => _mainStreamController.stream;

  void fetchUsers() async
  {
    UsersApi api = new UsersApi();
    viewData.doctors = await api.fetchAllDoctors();
    _sinkMainStreamData.add(viewData);
  }

  void fetchSlots() async
  {
    viewData.timeSlots = await api.fetchAllSlots();
    _sinkMainStreamData.add(viewData);
  }

  void setDate(DateTime pickedDate) 
  {
    viewData.selectedDate = pickedDate;
    _sinkMainStreamData.add(viewData);
  }

  void updateSelectedDoctor(User user)
  {
    viewData.selectedDoctor = user;
    _sinkMainStreamData.add(viewData);
  }

  void updateSelectedTimeSlot(Slot slot)
  {
    viewData.selectedTimeSlot = slot;   
    _sinkMainStreamData.add(viewData);
  }

  Future<bool> bookAppointment() async
  {
    return api.bookAppointment(viewData);
  }

  void showLoader()
  {
    viewData.loader = true;
    _sinkMainStreamData.add(viewData);
  }

  void hideLoader()
  {
    viewData.loader = false;
    _sinkMainStreamData.add(viewData);
  }

  @override
  void dispose() {
    _sinkMainStreamData.close();
  }

}