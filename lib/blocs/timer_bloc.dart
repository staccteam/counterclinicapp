import 'dart:async';

import 'package:counter_clinic/blocs/appointment_status_bloc.dart';
import 'package:counter_clinic/widgets/bloc_provider.dart';

class TimerBloc extends BlocBase
{
  int seconds = 0;
  Timer _timer;
  TimerBloc()
  {
    this._tickClock();
  }

  StreamController<String> _timerStreamController = new StreamController<String>.broadcast();
  Sink<String> get _inputStream => _timerStreamController.sink;
  Stream<String> get outputStream => _timerStreamController.stream;

  void _tickClock()
  {
    print("Timer Initialized: ${this.seconds}");
    if (this._timer != null)
      this._timer.cancel();

    this._timer = Timer.periodic(Duration(seconds: 1), (Timer t) async {
      print("Timer Ticking...");
      _inputStream.add("${this.seconds++}");
    });
  }

  @override
  void dispose() {
    _timerStreamController.close();
    _timer.cancel();
  }

}