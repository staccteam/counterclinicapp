import 'dart:async';

import 'package:counter_clinic/api/appointment_api.dart';
import 'package:counter_clinic/models/appointment.dart';
import 'package:counter_clinic/widgets/bloc_provider.dart';
import 'package:intl/intl.dart';

class AppointmentStatusPageViewData
{
  Appointment appointment;
  AppointmentQueueStatus prevAppointmentQueueStatus;
  AppointmentQueueStatus appointmentQueueStatus;
  bool showStatus = false;
  int timeRemainingInSeconds =  0;
  String appointmentStatus;
}

class AppointmentStatusBloc extends BlocBase 
{
  AppointmentStatusPageViewData viewData;
  Timer _timer;
  int _seconds = 0;
  int appointmentNumber = 0;
  bool isSet = false;
  Appointment patientAppointment;

  AppointmentStatusBloc(int appointmentNumber){
    this.appointmentNumber = appointmentNumber;
    viewData = new AppointmentStatusPageViewData();
    this.fetchAppointmentByNumber(appointmentNumber);

    if (appointmentNumber > 0)
    {
      
      this._timer = Timer.periodic(Duration(seconds: 1), (Timer t)  async {
      this._seconds++;
      // refresh current appointment state after every 5 seconds.
      if (this._seconds % 5 == 0)
      {
        viewData.prevAppointmentQueueStatus = viewData.appointmentQueueStatus;
        viewData.appointmentQueueStatus = await fetchAppointmentStatus(appointmentNumber);
        _updateAppointmentStatus(viewData.prevAppointmentQueueStatus, viewData.appointmentQueueStatus);
        
        if (!isSet)
        {
          this.viewData.timeRemainingInSeconds = _calcAvgTimeInSeconds(this.viewData.appointmentQueueStatus);
          isSet = true;
        }
          
        this._seconds = 0;
      }

      if (!(this.viewData.timeRemainingInSeconds <= 0))
      {
        this.viewData.timeRemainingInSeconds--;
        this._inputAppointmentStatus.add(this.viewData);
      }
    });

      
    }    
  }

  void _updateAppointmentStatus(AppointmentQueueStatus prevStatus, AppointmentQueueStatus status)
  {

    try
      {
        // Timer should not start until first patient is taken in
      if (status.currQueueCount == 0)
      {
        print("not start until first patient is taken in");
        this.viewData.showStatus = true;
        this.viewData.appointmentStatus = "Doctor has not started taking patients.";
        _inputAppointmentStatus.add(this.viewData);
      }

      // It should print the message 'It's your turn'
      else if (this.viewData.appointment.appointmentNumber == status.currQueueCount)
      {
        print("It's your turn");
        this.viewData.showStatus = true;
        this.viewData.appointmentStatus = "It's your turn.";
        _inputAppointmentStatus.add(this.viewData);
      }

      // It should print the message 'Your turn is over, please contact receptionist.'
      else if (this.viewData.appointment.appointmentNumber < status.currQueueCount)
      {
        print("Your turn is over, please contact receptionist.");
        this.viewData.showStatus = true;
        this.viewData.appointmentStatus = "Your turn is over, please contact receptionist.";
        _inputAppointmentStatus.add(this.viewData);
      }

      // It should print the message 'You are next, be ready!' and not the timer
      else if (this.viewData.appointment.appointmentNumber - status.currQueueCount == 1)
      {
        print("You are next, be ready!");
        this.viewData.appointmentStatus = "You are next, be ready!";
        this.viewData.showStatus = true;
        _inputAppointmentStatus.add(this.viewData);
      }

      // On pressing next patient the currQueue will change and so does the timer
      else if (prevStatus != null 
        && prevStatus.currQueueCount != status.currQueueCount)
      {
        print("On pressing next patient the currQueue will change and so does the timer");
        this.viewData.showStatus = false;
        this.viewData.timeRemainingInSeconds =  _calcAvgTimeInSeconds(status);
        this.viewData.appointmentStatus = "${this.viewData.timeRemainingInSeconds}";
        _inputAppointmentStatus.add(this.viewData);
      }

      // It should sync timer when doctor status changes
      else if (prevStatus != null 
        && prevStatus.doctorStatus != status.doctorStatus)
      {
        print("It should sync timer when doctor status changes!");
        this.viewData.showStatus = false;
        this.viewData.timeRemainingInSeconds =  _calcAvgTimeInSeconds(status);
        this.viewData.appointmentStatus = "${this.viewData.timeRemainingInSeconds}";
        _inputAppointmentStatus.add(this.viewData);
      }

      //  It should sync timer when the avg time calculation mode changes
      else if (prevStatus != null 
        && prevStatus.averageTimeCalculationLogic != status.averageTimeCalculationLogic)
      {
        print("It should sync timer when the avg time calculation mode changes");
        this.viewData.showStatus = false;
        this.viewData.timeRemainingInSeconds =  _calcAvgTimeInSeconds(status);
        this.viewData.appointmentStatus = "${this.viewData.timeRemainingInSeconds}";
        _inputAppointmentStatus.add(this.viewData);
      }

      // It should show message that doctor has logged out and the day has been ended
      else if  (status.doctorStatus.contains("LOGGED_OUT"))
      {
        print("Doctor has clicked logged out button and have been logged out.");
        this.viewData.showStatus = false;
        this.viewData.appointmentStatus = "Doctor has left for the day!";
        _inputAppointmentStatus.add(this.viewData);
      }

      else if (prevStatus != null && prevStatus.avgCheckupTime != status.avgCheckupTime)
      {
        this.viewData.showStatus = false;
        this.viewData.appointmentStatus = "Doctor is checking the patients";
        _inputAppointmentStatus.add(this.viewData);
      }

      else {
        String formatDisplayTime(Duration duration)
        {
          return "${new DateFormat('hh:mm a').format(new DateTime.now().add(duration))}";
        }

        print("Time remaining: ${viewData.timeRemainingInSeconds}");
        this.viewData.showStatus = false;
        this.viewData.appointmentStatus = "${formatDisplayTime(Duration(seconds: this.viewData.timeRemainingInSeconds))}";
        _inputAppointmentStatus.add(this.viewData);
      }
    }

    on Exception
    {
      
    }
  }

  /// calculates average time for the patient
  int _calcAvgTimeInSeconds(AppointmentQueueStatus status)
  {
    if (status.averageTimeCalculationLogic == "MANUAL")
      return (this.viewData.appointment.appointmentNumber - status.currQueueCount)  * Duration(milliseconds: status.avgCheckupTime).inSeconds + Duration(milliseconds: status.extendedTime).inSeconds;
    
    int approxCheckupTime  = 0;
    
    try {
      approxCheckupTime = (this.viewData.appointment.appointmentNumber - status.currQueueCount)  * (status.totalCheckupTime / status.currQueueCount).round() + Duration(milliseconds:  status.extendedTime).inSeconds;
    } 
    on Exception
    {
      print("There was exception while calculating approx checkup time. Appointment Status: $status");
    }
    
    return Duration(milliseconds: approxCheckupTime).inSeconds;
  }

  final AppointmentApi api = new AppointmentApi();  

  // Appointment Queue Status Stream
  StreamController<AppointmentStatusPageViewData> _appointmentStatusStreamController = new StreamController<AppointmentStatusPageViewData>.broadcast();
  Sink<AppointmentStatusPageViewData> get _inputAppointmentStatus => _appointmentStatusStreamController.sink;
  Stream<AppointmentStatusPageViewData> get outputStream => _appointmentStatusStreamController.stream;

  /// Fetches appointment status from the api
  Future<AppointmentQueueStatus> fetchAppointmentStatus(int appointmentNumber) async
  {
    AppointmentQueueStatus appointmentStatus = await api.fetchAppointmentQueueStatus(appointmentNumber);
    
    this.viewData.appointmentQueueStatus = appointmentStatus;
    _inputAppointmentStatus.add(this.viewData);

    this.appointmentNumber = appointmentNumber;
    return appointmentStatus;
  }

  /// fetches appointment by appointment number from the api
  Future<Appointment> fetchAppointmentByNumber(int appointmentNumber) async
  {
    this.patientAppointment = await api.fetchAppointmentByNumber(appointmentNumber);
    viewData.appointment = this.patientAppointment;
    _inputAppointmentStatus.add(this.viewData);
    return this.patientAppointment;
  }

  @override
  void dispose() {
    this._timer.cancel();
    _appointmentStatusStreamController.close();
  }

}