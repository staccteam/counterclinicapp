import 'dart:async';

import 'package:counter_clinic/api/appointment_api.dart';
import 'package:counter_clinic/models/AppointmentStatus.dart';
import 'package:counter_clinic/models/appointment.dart';
import 'package:counter_clinic/models/online_appointment.dart';
import 'package:counter_clinic/widgets/bloc_provider.dart';

class AppointmentBloc extends BlocBase
{

  AppointmentApi _api;
  AppointmentBloc()
  {
    _api = new AppointmentApi();
  }

  Timer _timer;

  StreamController<List<OnlineAppointment>> _onlineAppointmentController = new StreamController<List<OnlineAppointment>>.broadcast();
  Sink<List<OnlineAppointment>> get _inputOnlineAppointment => _onlineAppointmentController.sink;
  Stream<List<OnlineAppointment>> get outputOnlineAppointment => _onlineAppointmentController.stream;

  StreamController<AppointmentQueueStatus> _appointmentQueueStatusController = new StreamController<AppointmentQueueStatus>.broadcast();
  Sink<AppointmentQueueStatus> get _inputAppointmentQueueStatus => _appointmentQueueStatusController.sink;
  Stream<AppointmentQueueStatus> get outputAppointmentQueueStatus => _appointmentQueueStatusController.stream;

  StreamController<int> _tabStream =  new StreamController<int>.broadcast();
  Sink<int> get _inputTabIndex => _tabStream.sink;
  Stream<int> get outputTabIndex => _tabStream.stream;

  void setIndex(int index)
  {
    _inputTabIndex.add(index);
  }

  void fetchOnlineAppointments() async
  {
    print("Fetching Online Appointments");
    List<OnlineAppointment> onlineAppointments = await _api.fetchOnlineAppointmentByDoctorId();
    _inputOnlineAppointment.add(onlineAppointments);
  }

  // call service at given interval
  void updateDoctorQueueAtInterval(int intervalInSeconds)
  {
    _timer = Timer.periodic(Duration(seconds: intervalInSeconds), (t){
      fetchDoctorQueueSummary();
    });
  }

  /// Fetch appointment queue status for doctor with token
  Future<AppointmentQueueStatus>  fetchDoctorQueueSummary() async
  {
    print("Fetching Walk-In Queue Info");
    AppointmentQueueStatus appointmentStatus = await _api.fetchAppointmentQueueStatusByUserId();
    _inputAppointmentQueueStatus.add(appointmentStatus);
    return appointmentStatus;
  }

  @override
  void dispose() {
    if (_timer !=  null)
      _timer.cancel();
    
    _tabStream.close();
    _onlineAppointmentController.close();
    _appointmentQueueStatusController.close();
  }

}