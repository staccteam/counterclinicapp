import 'package:counter_clinic/pages/Screens.dart';
import 'package:counter_clinic/pages/appointment_status_screen.dart';
import 'package:counter_clinic/pages/contact_screen.dart';
import 'package:counter_clinic/pages/doctor_home_screen.dart';
import 'package:counter_clinic/pages/entry_screen.dart';
import 'package:counter_clinic/pages/home_page_screen.dart';
import 'package:counter_clinic/pages/my_appointments_screen.dart';
import 'package:counter_clinic/pages/offline_scan_screen.dart';
import 'package:counter_clinic/pages/support_screen.dart';
import 'package:counter_clinic/utils/StorageHandler.dart';
import 'package:counter_clinic/utils/Utils.dart';
import 'package:counter_clinic/models/AppointmentPayload.dart';
import 'package:flutter/material.dart';

Screens currentDrawerState = Screens.MAIN;
String jwtToken = "";
AppointmentPayload appointmentPayload;

Drawer getDoctorDashboardDrawer(BuildContext context) {
  Drawer drawer = Drawer(
    child: ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        DrawerHeader(
          child: getTextWithFontAndSize(
              text: "Counter Clinic",
              fontSize: MEDIUM_LABEL_FONT_SIZE,
              color: Colors.white),
          decoration: BoxDecoration(
            color: Colors.green,
          ),
        ),
        ListTile(
          title: getTextWithFontAndSize(
              prefix: Icon(
                Icons.scanner,
                color: Colors.brown,
                size: BIG_LABEL_FONT_SIZE,
              ),
              text: "Walk-In Patients",
              color: currentDrawerState == Screens.WALK_IN
                  ? Colors.green
                  : Colors.black),
          onTap: () => Navigator.push(context,
              MaterialPageRoute(builder: (context) => DoctorHomeScreen())),
          selected: currentDrawerState == Screens.WALK_IN,
        ),
      ],
    ),
  );
}

Drawer getDrawer(BuildContext context) {
  var drawerListItems = new List<Widget>();
  drawerListItems.add(
    DrawerHeader(
        child: Container(
          child: Text(
            "Counter Clinic",
            style: TextStyle(
                fontSize: MEDIUM_LABEL_FONT_SIZE, color: Colors.white),
          ),
          alignment: Alignment.bottomCenter,
        ),
        decoration: BoxDecoration(
            color: Colors.green, backgroundBlendMode: BlendMode.darken)),
  );

  drawerListItems.add(
    ListTile(
      leading: Icon(
        Icons.scanner,
        color: Colors.brown,
        size: BIG_LABEL_FONT_SIZE,
      ),
      title: getTextWithFontAndSize(
          text: "Scan QR Code",
          color:
              currentDrawerState == Screens.MAIN ? Colors.green : Colors.black),
      onTap: () => Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomePageScreen())),
      selected: currentDrawerState == Screens.MAIN,
    ),
  );

  drawerListItems.add(
    ListTile(
      leading: Icon(
        Icons.offline_bolt,
        color: Colors.brown,
        size: BIG_LABEL_FONT_SIZE,
      ),
      title: getTextWithFontAndSize(
          text: "Offline Scan",
          color: currentDrawerState == Screens.OFFLINE_SCAN
              ? Colors.green
              : Colors.black),
      onTap: () => Navigator.push(context,
          MaterialPageRoute(builder: (context) => OfflineScanScreen())),
      selected: currentDrawerState == Screens.OFFLINE_SCAN,
    ),
  );

  if (isUserLoggedIn)
    drawerListItems.add(
      ListTile(
          leading: Icon(
            Icons.star,
            color: Colors.brown,
            size: BIG_LABEL_FONT_SIZE,
          ),
          title: getTextWithFontAndSize(
              text: "My Appointments",
              color: currentDrawerState == Screens.MY_APPOINTMENTS
                  ? Colors.green
                  : Colors.black),
          onTap: () {
            _initAppointmentStatus().then((onValue) {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MyAppointments()));
            });
          },
          selected: currentDrawerState == Screens.MY_APPOINTMENTS),
                  
    );

  drawerListItems.add(
    ListTile(
        leading: Icon(
          Icons.star,
          color: Colors.brown,
          size: BIG_LABEL_FONT_SIZE,
        ),
        title: getTextWithFontAndSize(
            text: "Appointment Status",
            color: currentDrawerState == Screens.APPOINTMENT_STATUS
                ? Colors.green
                : Colors.black),
        onTap: () {
          _initAppointmentStatus().then((onValue) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => AppointmentStatusScreen(
                        jwtToken: jwtToken,
                        hisAppointmentStatus: appointmentPayload)));
          });
        },
        selected: currentDrawerState == Screens.APPOINTMENT_STATUS),
  );

  drawerListItems.add(
    ListTile(
      leading: Icon(
        Icons.contact_mail,
        color: Colors.brown,
        size: BIG_LABEL_FONT_SIZE,
      ),
      title: getTextWithFontAndSize(
          text: "Contact Us",
          color: currentDrawerState == Screens.CONTACT
              ? Colors.green
              : Colors.black),
      onTap: () => Navigator.push(
          context, MaterialPageRoute(builder: (context) => ContactScreen())),
      selected: currentDrawerState == Screens.CONTACT,
    ),
  );

  drawerListItems.add(
    ListTile(
      leading: Icon(
        Icons.help,
        color: Colors.brown,
        size: BIG_LABEL_FONT_SIZE,
      ),
      title: getTextWithFontAndSize(
          text: "Technical Support",
          color: currentDrawerState == Screens.SUPPORT
              ? Colors.green
              : Colors.black),
      onTap: () => Navigator.push(
          context, MaterialPageRoute(builder: (context) => SupportScreen())),
      selected: currentDrawerState == Screens.SUPPORT,
    ),
  );

  drawerListItems.add(Divider());

  drawerListItems.add(
    ListTile(
      leading: Icon(
        Icons.close,
        color: Colors.brown,
        size: BIG_LABEL_FONT_SIZE,
      ),
      title: getTextWithFontAndSize(text: "Close"),
      onTap: () => Navigator.of(context).pop(),
    ),
  );

  if(isUserLoggedIn)
    drawerListItems.add(
      ListTile(
        leading: Icon(
          Icons.exit_to_app,
          color: Colors.brown,
          size: BIG_LABEL_FONT_SIZE,
        ),
        title: getTextWithFontAndSize(text: "Logout"),
        onTap: () {
          deleteText(AUTH_TOKEN);
          deleteText(ACCESS_TOKEN);
          isUserLoggedIn = false;
          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=>new EntryScreen()));
        },
      )
    );

  Drawer drawer = Drawer(
    child: ListView(
      padding: EdgeInsets.zero,
      children: drawerListItems
    ),
  );
  return drawer;
}

Future<void> _initAppointmentStatus() async {
  jwtToken = await getText(JWT_TOKEN_STORAGE_KEY);
  appointmentPayload = AppointmentPayload.fromJson(
      await getText(APPOINTMENT_PAYLOAD_STORAGE_KEY));
}

/**
 * Display Toast at the bottom with the provided text
 */
void showSnackBarWithMessage(
    {BuildContext context,
    String msg,
    Color backgroundColor = Colors.black12,
    Duration duration = const Duration(seconds: 2)}) async {
  Scaffold.of(context).showSnackBar(SnackBar(
    content: Text(msg),
    backgroundColor: backgroundColor,
    duration: duration,
  ));
}

Widget getAppTheme(
    {String title = "Counter Clinic",
    Widget homeWidget,
    Color scaffoldBackgroundColor = Colors.white,
    Color primarySwatch = Colors.green}) {
  return MaterialApp(
      title: title,
      theme: ThemeData(
        primarySwatch: primarySwatch,
        scaffoldBackgroundColor: scaffoldBackgroundColor,
      ),
      home: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image:
                        AssetImage("assets/blue_grafica_design_background.jpg"),
                    fit: BoxFit.fill)),
            child: homeWidget,
          ),
        ],
      ));
}
