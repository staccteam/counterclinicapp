import 'dart:async';
import 'dart:convert';

import 'package:counter_clinic/api/api_constants.dart';
import 'package:counter_clinic/models/site_options.dart';
import 'package:http/http.dart' as http;

class SiteOptionsApi
{
  Future<SiteOptions> fetchAllSiteOptions() async
  {
    http.Response response = await http.get(Uri.encodeFull("$BASE_URL/api/option"));
    print(response.body);
    return SiteOptions.fromJson(json.decode(response.body));
  }
}