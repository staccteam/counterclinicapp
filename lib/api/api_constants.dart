import 'package:counter_clinic/utils/StorageHandler.dart';
import 'package:counter_clinic/utils/Utils.dart';

const String env = "LOCAL";
const String BASE_URL = env == "LOCAL"  ? "https://257a694b.ngrok.io" : "http://139.59.25.205:9990";


Future<String> getAccessToken() async
  {
    return getJsonValue((await getText(AUTH_TOKEN)), ACCESS_TOKEN);
  }