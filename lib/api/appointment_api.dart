import 'dart:async';
import 'dart:convert';

import 'package:counter_clinic/api/api_constants.dart';
import 'package:counter_clinic/blocs/create_appointment_bloc.dart';
import 'package:counter_clinic/models/appointment.dart';
import 'package:counter_clinic/models/online_appointment.dart';
import 'package:counter_clinic/models/slots.dart';
import 'package:http/http.dart' as http;

class AppointmentApi
{

  Future<Slots> fetchAllSlots() async
  {
    String accessToken = await getAccessToken();
    http.Response response = await http.get(Uri.encodeFull("$BASE_URL/api/calendar/all-slots?auth_token=$accessToken"));
    print(response.body);
    List<Slot>  slots = List<Slot>();
    List<dynamic> parsedJson = json.decode(response.body);
    slots =  parsedJson.map((slot)=>Slot.fromJson(slot)).toList();
    Slots slotList = Slots();
    slotList.slot = slots;
    return slotList;
  }

  Future<bool> bookAppointment(AppointmentPageViewData viewData) async
  {
    print("Selected Date: " + viewData.selectedDate.toIso8601String());
    String accessToken = await getAccessToken();
    Map<String, dynamic> jsonMap =  {
      'selectedDoctorId': viewData.selectedDoctor.id,
      'selectedDate': viewData.selectedDate.toIso8601String(),
      'selectedTimeSlotId': viewData.selectedTimeSlot.id
    };
    http.Response response =  await http.post(
      Uri.encodeFull("$BASE_URL/api/appointment/save"),
      headers: {
        "Content-Type":"application/json",
        'auth_token': '$accessToken'
      },
      body: json.encode(jsonMap) 
    );
    if (response.statusCode != 200)
      throw Exception("Error booking appointment!");
    
    print(response.body);
    return true;
  }

  Future<List<OnlineAppointment>> fetchOnlineAppointments() async
  {
    String authToken = await getAccessToken(); 
    http.Response response = await http.get(Uri.encodeFull("$BASE_URL/api/appointment?auth_token=$authToken"));
    // print(response.body);
    List<OnlineAppointment> onlineAppointments = List<OnlineAppointment>();
    List<dynamic> parsedJson = json.decode(response.body);
    parsedJson.forEach((f)=>
      onlineAppointments.add(OnlineAppointment.fromJson(f))
    );
    
    return onlineAppointments;
  }

  /// fetches online appointments of doctor
  Future<List<OnlineAppointment>> fetchOnlineAppointmentByDoctorId() async
  {
    String authToken  = await getAccessToken();
    String url = "$BASE_URL/api/appointment/online?auth_token=$authToken";
    print("Calling Url: $url");
    http.Response response = await http.get(Uri.encodeFull(url));
    // print(response.body);
    List<OnlineAppointment> onlineAppointments = List<OnlineAppointment>();
    List<dynamic> parsedJson = json.decode(response.body);
    parsedJson.forEach((f)=>
      onlineAppointments.add(OnlineAppointment.fromJson(f))
    );
    return onlineAppointments;
  }

  Future<AppointmentQueueStatus> fetchAppointmentQueueStatus(int appointmentNumber) async
  {
      http.Response response = await  http.get(Uri.encodeFull("$BASE_URL/api/appointmentQueue/status/$appointmentNumber"));
      return AppointmentQueueStatus.fromJson(json.decode(response.body));
  }

  Future<Appointment> fetchAppointmentByNumber(int appointmentNumber) async
  {
      http.Response response = await  http.get(Uri.encodeFull("$BASE_URL/api/appointment/number/$appointmentNumber"));
      String jsonResponse = response.body;
      print(jsonResponse);
      Map<String, dynamic> appointment = json.decode(jsonResponse);
      return Appointment.fromJson(appointment);
  }

  Future<AppointmentQueueStatus> fetchAppointmentQueueStatusByUserId() async
  {
      String accessToken = await getAccessToken();
      http.Response response = await http.get(Uri.encodeFull("$BASE_URL/api/appointmentQueue/user?auth_token=$accessToken"));
      String jsonResponse = response.body;
      print(jsonResponse);
      Map<String, dynamic> appointmentStatus = json.decode(jsonResponse);
      return AppointmentQueueStatus.fromJson(appointmentStatus);
  }

}