import 'dart:convert';

import 'package:counter_clinic/api/api_constants.dart';
import 'package:counter_clinic/models/user.dart';
import 'package:counter_clinic/utils/StorageHandler.dart';
import 'package:counter_clinic/utils/Utils.dart';
import 'package:http/http.dart' as http;

class UsersApi 
{
  Future<String> getAccessToken() async
  {
    return getJsonValue((await getText(AUTH_TOKEN)), ACCESS_TOKEN);
  }

  Future<List<User>> fetchAllDoctors() async
  {
    String accessToken = await getAccessToken();
    http.Response  response = await http.get(
      Uri.encodeFull("$BASE_URL/api/user/doctors?access_token=$accessToken"));
    print(response.body);
    List<User> doctors = List<User>();
    List<dynamic> parsedJson = json.decode(response.body);
    doctors = parsedJson.map((item)=>User.fromJson(item)).toList();
    return doctors;
  }
}